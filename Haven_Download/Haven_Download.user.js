﻿// ==UserScript==
// @name        Haven Download
// @namespace   none
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @include     *aihaven.org/*
// @version     2015.12.02
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/Haven_Download/Haven_Download.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/Haven_Download/Haven_Download.user.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var $els = $("#brick-wrap div.brick .social-info");

if ($els.length) {
    $els.before("<a class='download-link' style='font-size:20px;cursor:pointer;'>DOWNLOAD</a>");

    $("#brick-wrap").on("click", ".download-link", function() {
        var url = $(this).parents(".brick.zoe-grid").find("a.thumbnail-image").attr("href");
        var s = url.indexOf("/?");
        if (s != -1) {
            url = url.substring(0, s) + "/";
        }
        console.log(url)
        SimpleHtmlParser.get(url, function(p) {
            if (p.containsTextThenSkip("btn btn-1 btn-1e")) {
                var vidUrl = p.getAttributeInCurrentTag("href");
                if (vidUrl) {
                    window.open(vidUrl);
                    return;
                }
            }
            alert("Cannot find link :( " + url);
        });
    });
}