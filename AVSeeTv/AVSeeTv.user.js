// ==UserScript==
// @name     AVSeeTv
// @namespace   none
// @include     https://avsee01.tv*
// @include     https://avsee01.tv
// @version  2018.9.15
// @grant    GM.xmlHttpRequest
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// ==/UserScript==

const USER="someguy444"
const PASS="pokemon"

const VIDEO_ROOT_URL = "http://cdn.redecanais.ru/";

function start() {
  let $login = $("#mb_id");
  if ($login.length) {
    $login.val(USER);
    $("#mb_password").val(PASS);
    //$("#miso_sidelogin").submit();
    return;
  }
   
  // Move the search bar
  let $search = $("form[name='allsearch']").css("float", "left").width(1000);
  $(".sidebar-toggle").after($search)
  
  
  
  // Resize video
  let $iframe = $("div.view-content[itemprop='description'] > iframe");
  
  // In Video page
  if ($iframe.length) {
  	$iframe.height(900);
    $iframe.width(1600);
    let $container = $iframe.parent();
    
    let $img = $(".view_image > img");
    let imgSrc = $img.attr("content");
    alert( imgSrc)
    $img.remove();
    
    let videoSrc = $iframe.attr("src")
    
    
    // Move iframe above the content
    $container.prepend($iframe);
    
    // Add Download button
    let link = $iframe.attr("src");
    if (link.includes("1080=") && link.includes(".xyz/")) {
      let videoSrc = VIDEO_ROOT_URL + link.substring(link.lastIndexOf(".xyz/") + 5);      
      $(".panel-heading > div").append($("<span>").addClass("sp"))
    		.append($("<a>").text("Right Click and Download").attr("target", "_blank").attr("href", videoSrc).attr("download", "vode.mp4"));
      
    } else {
      //alert("Cannot find video 1080p source: " + link);
      return;
    }
    $container.css("position", "relative").prepend(
      $("<div>").css({
        width: 1600, 
        height: 900, 
        position: "absolute", 
        top: 0, 
        left: 0,
        "background-image": "url('" + imgSrc + "')",
        "background-size": "cover"
      }).click(function() {
        $(this).hide();
      })
    );
  }
  window.scrollTo(0,0)
}


function removeAds() {
  $(".nivoSlider, .main-p10, #thema_footer, .div-sep-wrap").remove();
  
  $("#at-main > a[target='_blank'] > img[alt='graph']").remove();
  
  $("div.view-padding > a[target='_blank'] > img").remove();
  
}

removeAds();
start();
