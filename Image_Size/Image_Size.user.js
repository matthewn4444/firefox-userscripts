// ==UserScript==
// @name        Image Size
// @namespace   none
// @include     *.jpg
// @include     *.jpeg
// @include     *.jpg?*
// @include     *.jpg:*
// @include     *.jpeg?*
// @include     *.png
// @include     *.png?*
// @include     *.png:*
// @version     2015.7.11
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/876bfbef939e81b6113129efb902dfc3b8c3bfa1/Image_Size/Image_Size.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/876bfbef939e81b6113129efb902dfc3b8c3bfa1/Image_Size/Image_Size.user.js
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var src = $("img").attr("src");

var img = new Image();
img.onload = function() {
    $("body").append(
        $("<section>").text(img.width + "x" + img.height).css({
            background: 'rgba(0, 0, 0, 0.5)',
            padding: '10px',
            color: 'white',
            position: 'fixed',
            top: 0,
            right: 0,
            'font-family': 'Poppins' ,
        }).fadeIn()
    );
}
img.src = src;

$("img").click(function() {
  $(this).toggleClass("zoomed");
})

BRIDGE.css({
  "img.zoomed": {
    width: "100%",
    height: "100%",
    "object-fit": "contain",
  }
})


