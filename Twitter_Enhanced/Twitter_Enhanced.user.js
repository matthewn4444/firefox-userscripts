// ==UserScript==
// @name        Twitter Enhanced
// @namespace   matthewn4444
// @include     https://twitter.com/*
// @version     2015.1.1
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/Twitter_Enhanced/Twitter_Enhanced.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/Twitter_Enhanced/Twitter_Enhanced.user.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var forward = $("a.link:contains('Continue')");
if (forward.length) {
    var link = forward.attr("href");
    window.location.href = link;
}

BRIDGE.css({
    ".GalleryNav--next" : {
        "width" : "25% !important",
    },
  	"#timeline":  {
      	"box-shadow": "0 0 8px rgba(0,0,0,0.5)",
  	},
  	".stream-item:not(.no-header-background-module)": {
      	"background-color": "rgba(255,255,255,0.8)",
    },
  
});

