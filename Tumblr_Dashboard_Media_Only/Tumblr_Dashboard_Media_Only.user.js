﻿// ==UserScript==
// @name        Tumblr Dashboard Media Only
// @namespace   none
// @include     https://www.tumblr.com/dashboard
// @include     https://www.tumblr.com/dashboard/*
// @version     2015.12.2
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/11715a82aad2ff411746858220dc0b4958f7008d/Tumblr_Dashboard_Media_Only/Tumblr_Dashboard_Media_Only.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/11715a82aad2ff411746858220dc0b4958f7008d/Tumblr_Dashboard_Media_Only/Tumblr_Dashboard_Media_Only.user.js
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var working = false;

function removeUselessPosts() {
    if (working) return;
    working = true;
    var id = "this_is_the_last_item_so_far";
    var $posts = $("#" + id).nextAll(".post_container");
    if (!$posts.length) {
        $posts = $("li.post_container");
    } else {
        $("#" + id).removeAttr("id");
    }
    if ($posts.length) {
        var lastValidPost = null;
        $posts.each(function(){
            var $self = $(this);
            if (!$self.find("img, video").length) {
            }
            
            // If no video and not avatar image, then remove it
            if (!$self.find("video").length) {
                var $images = $self.find("img");
                var hasValidImage = false;
                for (var i = 0; i < $images.length; i++) {
                    var src = $images.eq(i).attr('src');
                    if (src && !src.contains("avatar_") && !src.contains("_avatar/")) {
                        hasValidImage = true;
                        lastValidPost = $self;
                        break;
                    }
                }
                $self.find("img.inline_external_image").each(function() {
                    $(this).attr("src", $(this).attr("external_src")).addClass("enlarged loaded");
                });
                if (!hasValidImage) {
                    $self.remove();
                }
            } else {
                lastValidPost = $self;
            }
        });
        if (lastValidPost) {
            lastValidPost.attr("id", id);
        }
    }
    working = false;
}

function start() {
    // Clean up 
    $("#new_post_buttons, .controls_section.controls_section_radar, #tumblr_radar").remove();
    removeUselessPosts();
    
    //$(window).scroll(function() {
    //    removeUselessPosts();
    //});
}




start();
