// ==UserScript==
// @name        MangaReader
// @namespace   none
// @include     *tai.net/
// @include     *tai.net/g/*
// @include     *tai.net/search*
// @include     *tai.net/tagged*
// @include     *tai.net/parody*
// @include     *tai.net/artist*
// @include     *tai.net/group*
// @include     *tai.net/character*
// @include     *tai.net/tag*
// @include     *tai.net/?page=*
// @version     2015.12.2
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/876bfbef939e81b6113129efb902dfc3b8c3bfa1/MangaReader/MangaReader.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/876bfbef939e81b6113129efb902dfc3b8c3bfa1/MangaReader/MangaReader.user.js
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @require     https://raw.githubusercontent.com/blasten/turn.js/master/turn.min.js
// @require     https://raw.githubusercontent.com/timmywil/jquery.panzoom/master/dist/jquery.panzoom.min.js
// @require     https://raw.githubusercontent.com/jquery/jquery-mousewheel/master/jquery.mousewheel.min.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var CONSTANTS = {
    SHOW_MANGA: false,
    BOOK_MARGIN: 20,            // Margin for the book to be displayed
    PAGE_DIM_SAMPLE: 6,         // Sample a page to find an appropriate the width and height
    ZOOM_TURN_PADDING: 40,      // When turn page and zoomed in, it will pan to the left top corner with this padding
};

// Globals variables per book
var $gContent, $gWrapper, $gContainer, $gManga, $zoomDisplay, gTotalPages, gUrlBase, gImgWidth, gImgHeight,
    gInZoomMode, gZoomValue, gFSZoomThreshold;

function start() {
    // Set most of the global variables up
    gInZoomMode = false;
    $gContent = $("#content");
    $gWrapper = $("<div>").attr("id","manga-wrapper");
    $gContainer = $("<div>").attr("id","manga-container").addClass("spinner");
    $gContent.prepend($gContainer.append($gWrapper));
    $zoomDisplay = $("<div>").attr("id", "zoom-display").text("100%").appendTo($(document.body));
    gTotalPages = $("#thumbnail-container a").length;
    gUrlBase = $("#cover img").attr("data-src");
    gUrlBase = gUrlBase.substring(0, gUrlBase.lastIndexOf("/") + 1).replace("t.n", "i.n");

    // Preload 1st page and another page to get the book's height and width
    preload(gUrlBase + "1.jpg", gUrlBase + "1.png", function(coverImg) {
        var testPage = Math.min(gTotalPages, CONSTANTS.PAGE_DIM_SAMPLE);
        preload(gUrlBase + testPage + ".jpg", gUrlBase + testPage + ".png", function(img) {
            gImgWidth = img.width;
            gImgHeight = img.height;

            // Create the manga book
            $gManga = $("<div>").attr("id", "manga").append(createPage(1));
            $gWrapper.append($gManga);
            $gContainer.css("background-color", "#999").bind("transitionend webkitTransitionEnd", function(e) {
                // Do initial animation and then no more
                $gContainer.css("transition", "none");
                $gContainer.unbind("transitionend webkitTransitionEnd");
                $gManga.turn("disable", false);
            }).removeClass("spinner");

            // Init turnjs
            var size = resize();
            $gManga.turn({
                acceleration: true,
                elevation: 150,
                width: size[0],
                height: size[1],
                gradients: true,
                autoCenter: true,
            }).fadeIn("slow").turn("disable", true);

            // Add the rest of the pages
            for (var i = 2; i <= gTotalPages; i++) {
                // Add page number on every page except last if is odd
                $gManga.turn("addPage", createPage(i, i != gTotalPages || i & 0x1));
            }

            // Apply transition without initial animation
            $gManga.addClass("turn").css("margin-left");
            $gManga.css('transition', 'margin-left 0.2s ease-in-out 0s');

            // Add exit zoom mode
            var $exitBtn = $("<div>").attr("id", "zoom-mode-exit").html("&#10006;");
            $(document.body).append($exitBtn);
            $exitBtn.on("click", function() {
                if (document.mozFullScreen) {
                    document.mozCancelFullScreen();
                } else {
                    toggleZoomMode();
                }
            });

            // Add events
            $(window).on("resize", resize);
            $(window).keypress(function(e){
                switch (e.keyCode) {
                    case 37:    // Left Arrow
                        $gManga.turn("previous");
                        break;
                    case 39:    // Right Arrow
                        if (gInZoomMode && gZoomValue > gFSZoomThreshold && $gManga.turn("page") != 1) {
                            // Pan to the right side if not there
                            var width = Math.round($gManga.outerWidth()) * gZoomValue;
                            var offset = $gWrapper.offset();
                            var moveLeft = -offset.left - (width - $(window).width());
                            if (-moveLeft > 0) {
                                $gWrapper.panzoom("pan", moveLeft - CONSTANTS.ZOOM_TURN_PADDING,
                                    Math.round(-offset.top + CONSTANTS.ZOOM_TURN_PADDING),
                                    {animate: true, relative:true});
                                return;
                            }
                        }
                        $gManga.turn("next");
                        break;
                    case 38:    // Up Arrow
                        if (gInZoomMode) {
                            var offset = $gWrapper.offset();
                            $gWrapper.panzoom("pan", 0,
                                Math.round(-offset.top + CONSTANTS.ZOOM_TURN_PADDING),
                                {animate: true, relative:true});
                        }
                        break;
                    case 40:    // Down Arrow
                        if (gInZoomMode) {
                            var height = Math.round($gWrapper.outerHeight()) * gZoomValue,
                                offset = $gWrapper.offset();
                            $gWrapper.panzoom("pan", 0,
                                Math.round(-offset.top - (height - $(window).height()) - CONSTANTS.ZOOM_TURN_PADDING),
                                {animate: true, relative:true});
                        }
                        break;
                }
            });
            $(".thumb-container").on("click", function() {
                var i = $(this).index() + 1;
                var body = $("html, body");
                body.animate({scrollTop:0}, '500', 'swing', function(){
                    setTimeout(function(){      // Less lag
                        $gManga.turn("page", i);
                    }, 0);
                });
            });
            $gManga.on("click", ".page", toggleZoomMode).on("turned", function() {
                if (gInZoomMode && gFSZoomThreshold) {
                    if (gZoomValue > gFSZoomThreshold) {
                        //$gWrapper.css({left:0, top:0});
                        var offset = $gWrapper.offset();
                        $gWrapper.panzoom("pan",
                            Math.round(-offset.left + CONSTANTS.ZOOM_TURN_PADDING),
                            Math.round(-offset.top + CONSTANTS.ZOOM_TURN_PADDING),
                            {animate: true, relative:true}
                        );
                    }
                }
            });
        });
    });

    // Cleanup
    $(".gallerythumb").removeAttr("href");
}

function toggleZoomMode(e) {
    if (gInZoomMode) {
        // Go back to normal
        gInZoomMode = false;
        $zoomDisplay.removeClass("show");
        $gWrapper.panzoom("destroy").css("transform", "none");
        $gContainer.off('mousewheel.focal').css("background-color", "#999");
        $gManga.off("start");
        $(window).on("resize", resize);
        $gManga.on("click", ".page", toggleZoomMode);
        resize();
        $(document).scrollTop(0);
    } else {
        gInZoomMode = true;

        // Zoom and position where it was clicked
        if (e) {
            var xFromEl = e.clientX,
                yFromEl = e.clientY,
                xFromOrigin = e.screenX,
                yFromOrigin = e.screenY,
                xOffset = $gWrapper.offset().left,
                yOffset = $gWrapper.offset().top,
                scaleFactor = gImgHeight / $(window).height();
            gFSZoomThreshold = 1 / scaleFactor;
            var left = (xFromOrigin + Math.round((xOffset - xFromEl)* scaleFactor));
            var top = (yFromOrigin + Math.round((yOffset - yFromEl)* scaleFactor));
            $gWrapper.css({"left": left, "top": top});
            gZoomValue = 1;
        }

        $gManga.off("click", ".page", toggleZoomMode);
        $gManga.turn("size", gImgWidth * 2, gImgHeight).turn("resize");
        $(window).off("resize");

        // Fullscreen
        document.body.mozRequestFullScreen();
        $(document).on("mozfullscreenchange", function(e) {
            if (!document.mozFullScreen) {
                $(document).off("mozfullscreenchange");
                toggleZoomMode();
            }
        });

        // Disable the corners
        $gManga.on("start", disableCorners);

        // Init the panzoom/scrollwheel library
        $gWrapper.panzoom().on("panzoomstart", function() {
            $gWrapper.addClass("grabbing");
        }).on("panzoomend", function() {
            $gWrapper.removeClass("grabbing");
        });
        $gContainer.on('mousewheel.focal', function(e) {
            e.preventDefault();
            var d = e.delta || e.originalEvent.wheelDelta;
            var zoomOut = d ? d < 0 : e.originalEvent.deltaY > 0;
            $gWrapper.panzoom('zoom', zoomOut, {
                increment: 0.1,
                animate: false,
                focal: e
            });
            gZoomValue = Math.min(Math.max(0.4, gZoomValue + (zoomOut ? -0.1 : 0.1)), 5);
            showDisplay();
        }).css("background-color", "#222");

        //manga-container
    }
    $(document.body).toggleClass("zoom-mode");
}

function showDisplay() {
    if (gInZoomMode) {
        var timer = $zoomDisplay.data("timer");
        $zoomDisplay.text(Math.round(gZoomValue * 100) + "%");
        $zoomDisplay.addClass("show");
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function(){
            $zoomDisplay.removeClass("show");
            $zoomDisplay.removeData("timer");
        }, 1000);
        $zoomDisplay.data("timer", timer);
    } else {
        $zoomDisplay.removeClass("show");
    }
}

function disableCorners(e, o, c) {
    if (c == 'tl' || c == 'tr' || c == 'bl' || c == 'br') e.preventDefault();
}

function createPage(n, hasPageNumber) {
    var $page = $("<div>")
       .css("background-image", "url(" + gUrlBase + n +  ".jpg), url(" + gUrlBase + n +  ".png)")
       .append($("<img/>").attr("src", gUrlBase + n +  ".jpg"));
    if (hasPageNumber) {
        $page.append($("<aside>").addClass("page-number").text(n - 1));
    }
    return $page;
}

function preload(url, fallbackUrl, callback) {
    var img = new Image();
    var triedFallback = false;
    img.onload = function() {
        if (callback) callback.call(this, img);
    }.bind(this);
    img.onerror = function() {
        if (triedFallback) {
            alert("Failed to preload image!");
        } else {
            triedFallback = true;
            img.src = fallbackUrl;
        }
    }.bind(this);
    img.src = url;
}

function resize() {
    // Calculate how big the book should be
    var $window = $(window);
    var allowedHeight = ($window.height() - $("#navbar-container").height()) - CONSTANTS.BOOK_MARGIN * 2;
    var allowedWidth = $window.width() - CONSTANTS.BOOK_MARGIN * 2;
    var imageWidth = gImgWidth * 2;
    var allowedAspectRatio = allowedWidth / allowedHeight;
    var imgAspectRatio = imageWidth / gImgHeight


    var width = Math.round(allowedAspectRatio > imgAspectRatio ?
        imageWidth / gImgHeight * allowedHeight : allowedWidth);
    var height = Math.round(allowedAspectRatio > imgAspectRatio ?
        allowedHeight : gImgHeight / imageWidth * allowedWidth);

    // Set the wrapper's dimensions
    $("#manga-wrapper").css("width", width);
    $("#manga-container").css("height", height + CONSTANTS.BOOK_MARGIN * 2);

    // Set the size and resize the manga book
    var $gManga = $("#manga.turn");
    if ($gManga.length) {
        $gManga.css("transition", "");
        $gManga.turn("size", width, height).turn("resize");
        $gManga.css("transition", 'margin-left 0.2s ease-in-out');
    }
    return [width, height];
}

function addCssText(text) {
    $("head").append($("<style>").text(text));
}

function applyCSS() {
BRIDGE.css({
    'body': {
        'overflow-x': 'hidden',
    },

    ".gallerythumb:hover": {
        "cursor": 'pointer',
    },
    ".gallerythumb:active": {
        "opacity": '0.7',
    },

    "#manga-container": {
        'padding': CONSTANTS.BOOK_MARGIN + "px 0",
        'width': '100%',
        'background-color': '#ccc',
        'transition': 'background-color 1s ease-in-out 0s',
        'margin-bottom': '20px',
        'overflow': 'hidden',
        'transition': 'height 0.5s ease-in-out 0s',
        'height': '200px',
        'background-repeat': 'no-repeat',
        'background-position': 'center',
    },

    "nav.navbar.navbar-default": {
        'margin-bottom': '0',
    },

    "#content": {
        'width': '100%',
    },

    '#manga-wrapper': {
        'margin': '0 auto',
    },

    "#manga .shadow": {
        'box-shadow': '0 0 5px 3px #555',
    },

    "#manga .page-wrapper": {
        '-webkit-perspective': '2000px',
        '-moz-perspective': '2000px',
        'perspective': '2000px',
    },

    "#manga .page": {
        'background-size': '100%',
        'background-color': 'white',
    },

    "#manga .page img": {
        'opacity': '0 !important'
    },

    ".page-wrapper .odd": {
        '-webkit-box-shadow': 'inset 0 0 5px #666',
        '-moz-box-shadow': 'inset 0 0 5px #666',
        'box-shadow': 'inset 0 0 5px #666',
    },

    ".page-wrapper .p1.odd": {
        'box-shadow': '0 0 transparent',
    },

    ".page-wrapper .page-number": {
        'margin': '4%',
        //'font-weight': 'bold',
        'font-family': 'Century Gothic, Verdana',
        'color': 'white',
        'position': 'absolute',
        'background': '#222',
        'width': '30px',
        'height': '30px',
        'border-radius': '15px',
        'line-height': '30px',
        'bottom': 0
    },

    ".page-wrapper .even .page-number": {
        'left': 0
    },

    ".page-wrapper .odd .page-number": {
        'right': 0
    },

    ".page-wrapper .even": {
        '-webkit-box-shadow': 'inset 0 0 5px #666',
        '-moz-box-shadow': 'inset 0 0 5px #666',
        'box-shadow': 'inset 0 0 5px #666',
    },

    // Zoom mode exit button
    "#zoom-mode-exit": {
        'color': 'white',
        'position': 'fixed',
        'top': '0',
        'right': '0',
        'width': '50px',
        'height': '50px',
        'transition': 'background-color 0.2s ease-in-out 0s',
        'border-bottom-left-radius': '50px',
        'background-color': 'rgba(0,0,0,0.7)',
        'display': 'none',
        'z-index': '101',
        'padding-left': '10px',
        'line-height': '40px',
        'cursor': 'pointer',
        'user-select': 'none',
    },

    "#zoom-mode-exit:hover": {
        'background-color': 'rgba(0,0,0,0.5)',
    },

    "#zoom-mode-exit:active": {
        'background-color': 'rgba(0,0,0,0.9)',
    },

    // Zoom Mode
    'body.zoom-mode': {
        'overflow': 'hidden',
    },

    "body.zoom-mode #manga": {
        'transition': 'none !important',
    },

    "body.zoom-mode #manga-wrapper": {
        'position': 'absolute',
        'margin': 0,
        'top': 0,
        'left': 0,
        'cursor': 'grab !important',
        'transition': 'top 0.5s ease-in-out 0s, left 0.5s ease-in-out 0s',
    },

    "body.zoom-mode #manga-wrapper.grabbing": {
        'cursor': 'grabbing !important',
    },

    "body.zoom-mode #manga-container": {
        "position": "fixed !important",
        'z-index': '100',
        "top": 0,
        "left": 0,
        "right": 0,
        "bottom": 0,
        'margin': 0,
        'height': 'auto !important',
    },

    "body.zoom-mode nav.navbar.navbar-default": {
        'margin-top': '-50px',
    },

    "body.zoom-mode #zoom-mode-exit": {
        'display': 'block',
    },

    "body.zoom-mode #manga .shadow": {
        'box-shadow': '0 0 20px 5px #111',
    },

    // Zoom Display
    "#zoom-display" : {
        "position": "fixed",
        "top": '20px',
        'left': '50%',
        'transform': 'translateX(-50%)',
        'color': 'white',
        'background-color': 'rgba(0,0,0,0.7)',
        'border-radius': '15px',
        'text-align': 'center',
        'padding': '15px 20px',
        'opacity': 0,
        'pointer-events': 'none',
        'transition': 'opacity 0.5s ease-out 0s',
        'line-height': '25px',
        'z-index': '200',
        'font-size': '35px',
        'font-family': 'Century Gothic, Verdana, Arial',
        'box-shadow': '0 0 10px 5px rgba(0,0,0,0.4)',
    },
    "#zoom-display.show": {
        'opacity': 1,
    },
});
}

// Start
$("body").css("background", "none")
if ($("#bigcontainer").length) {
    applyCSS();
    if (CONSTANTS.SHOW_MANGA) {
        start();
    }
    $("#page-container, #mobile-page, footer").remove();
} else if ($(".gallery-thumbnail, .preview-container").length) {
    $(".gallery-thumbnail a, .preview-container a").attr("target", "_black");
} else if (window.location.href.includes("/search/") || window.location.href.includes("/artist/")
        || window.location.href.includes("/group/")|| window.location.href.includes("/tag/")
        || window.location.href.includes("?page=")|| window.location.href.includes("/")) {
    $("#content .gallery .cover").each(function() {
        var href = $(this).attr("target", "_blank").attr("href");
        var id = href.substring(3, href.length - 1);
        $(this).find(".caption").append(
            $("<br>")
        ).append(
            $("<a>").addClass("btn btn-secondary").attr("href", "/g/" + id + "/download").append(
                $("<i>").addClass("fa fa-download")
            ).append(" Download")
        );
    });
}

// Handle keypress
var $next = $(".next");
var $prev = $(".previous");
if ($next.length || $prev.length) {
    $(document).keypress(function(event) {
        switch(event.keyCode) {
            case 39:
                if ($next.length) {
                    window.location.href = $next.attr("href");
                }
                break;
            case 37:
                if ($prev.length) {
                    window.location.href = $prev.attr("href");;
                }
                break;
        }
    })
}
