// ==UserScript==
// @name        jzoo
// @namespace   none
// @version     2015.12.02
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/jzoo/jzoo.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/jzoo/jzoo.user.js
// @include     *avmo.pw/*
// @include     *javlog.com/*
// @grant       none
// ==/UserScript==

$("body").removeClass("modal-open");
$("#blockModal, .modal-backdrop").remove();
