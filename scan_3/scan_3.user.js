// ==UserScript==
// @name        scan <3
// @namespace   none
// @version     2014.12.20
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/scan_3/scan_3.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/scan_3/scan_3.user.js
// @include     http://forum.scanlover.com/forumdisplay.php?f=*
// @include     http://forum.scanlover.com/forumdisplay.php?s=*
// @include     http://forum.scanlover.com/attachmentshow.php?attachmentid*    
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var IMAGE = {
    put: function(url, imgArr) {
        sessionStorage.setItem(url, imgArr.join(","));
    },
    get: function(url) {
        var items = sessionStorage.getItem(url);
        return items ? items.split(",") : [];
    }
};

function startThreadImages() {
    var trs = $("#threadslist > tbody:eq(1) > tr:gt(1)");
    
    for (var i = 0; i < trs.length; i++) {
        var aEl = trs.eq(i).find("a[id*='thread_title_']");
        loadImages(aEl);
    }
}

function loadImages(aEl) {
    aEl.attr("target", "_blank");
    var parent = aEl.parent().parent();
    var url = aEl.attr("href");
    
    function putToPage(imgUrls) {
        parent.append("<br/>");
        for (var i = 0; i < imgs.length; i++) {
            var largeImg = "http://forum.scanlover.com/attachment.php?attachmentid="
                + imgs[i].between("/", ".", imgs[i].lastIndexOf("/"));
            parent.append(
                $("<a>").attr("href", largeImg).attr("target", "_blank").append(
                    $("<img>").attr("src", imgs[i])
                )
            );
        }
    }

    // Get images
    var imgs = IMAGE.get(url);
    if (imgs.length) {
        putToPage(imgs);
    } else {
        SimpleHtmlParser.get(url, function(parser){
            var sectionText = parser.html.between("<!-- attachments -->", "<!-- / attachments -->");
            parser = new SimpleHtmlParser(sectionText);
            var inf = 0;
            while(parser.containsTextThenSkip(['class="thumbnail" src="'])) {
                if (inf++ > 1000){
                    alert("infinite loop")
                    return;
                }
                imgs.push(parser.getAttributeInCurrentTag("src"));
                parser.position += 10;      // TODO fix this, this is a hack
            }
            // Attach the images to page
            if (imgs.length) {
                IMAGE.put(url, imgs);
                putToPage(imgs);
            }
        });
    }
}

var here = window.location.href;
if (here.contains("attachmentshow.php?")) { 
    window.location.href = here.replace("attachmentshow.php", "attachment.php");
} else {
    startThreadImages();
}



BRIDGE.css({
    
});