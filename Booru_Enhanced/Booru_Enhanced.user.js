﻿// ==UserScript==
// @name        Booru Enhanced
// @namespace   matthewn4444
// @version     2015.2.17
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/c4f0d30bb9633dcb8ffca0f64fd9d7c04b7eb43a/Booru_Enhanced/Booru_Enhanced.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/c4f0d30bb9633dcb8ffca0f64fd9d7c04b7eb43a/Booru_Enhanced/Booru_Enhanced.user.js
// @include     http://danbooru.donmai.us*
// @include     *donmai.us*
// @include     https://yande.re/pos*
// @include     http://e621.net/pos*
// @include     http://rule34.xxx/*
// @include     http://gelbooru.com/*
// @include     https://konachan.com/*
// @include     http://xbooru.com/*
// @include     http://konachan.com/pos*
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @version     1
// @grant       GM_addStyle
// ==/UserScript==

/**
 *  CONSTANTS
 */
var URL             = window.location.href,
    CURRENT_PAGE    = null,
    THUMBS_MODE     = null,             //Bool
    BODY            = document.body;

$.fn.exists = function(){
    return this.length > 0;
}

String.prototype.contains = function(str) {
    return this.indexOf(str) != -1;
}

if (URL.contains("/posts/" && URL.contains("https:"))) {
    window.location.href = URL.replace("https", "http");
    return;
}

/**
 *  PAGES
 */
var PAGES = {
    DANBOORU: {
        name:           'danbooru',
        url_sel:        'donmai.us',
        thumb_sel:      ".post-preview a",
        resize_sel:     "#",
        comments_sel:   "#comments form",
        extra: function() {
            $("#tos-notice, #sign-up-notice").remove();
        }
    },
    KONACHAN: {
        name:           'konachan',
        url_sel:        'konachan.com',
        thumb_sel:      "#post-list-posts a.thumb",
    },
    XBOORU: {
        name:           'xbooru',
        url_sel:        'xbooru.com',
        thumb_sel:      "#post-list span.thumb a",
    },
    GELBOORU: {
        name:           'gelbooru',
        url_sel:        'gelbooru.com',
        thumb_sel:      "#post-list div.content span.thumb a",
        comments_sel:   "div#comments > div",
    },
    YANDE: {
        name:           'yande.re',
        url_sel:        'yande.re',
        thumb_sel:      "#post-list-posts a.thumb",
        comments_sel:   "div#comments > div",
    },
    E621: {
        name:           'e621',
        url_sel:        'e621.net',
        thumb_sel:      "#post-list span.thumb a[onclick]",
    },
    RULE34: {
        name:           'rule34',
        url_sel:        'rule34.xxx',
        thumb_sel:      "#post-list span.thumb a",
    },
};
// Select the page
for (var site in PAGES) {
    if (URL.contains(PAGES[site].url_sel)) {
        CURRENT_PAGE = PAGES[site];
    }
}

/**
 *  CLEANUP
 */
if (CURRENT_PAGE != null) {
    var links = $(CURRENT_PAGE.thumb_sel);
    THUMBS_MODE = links.exists();
    $(BODY).addClass(CURRENT_PAGE.name);

    // Add tab links to the gallery thumbs
    if (THUMBS_MODE) {
        links.each(function(){
            $(this).attr("target", "_blank").removeAttr("onclick");
        });
    }

    $('#sidebar ul li a, .sidebar ul li a').attr("target", "_blank");

    // Remove the comments box
    if (CURRENT_PAGE.hasOwnProperty("comments_sel")) {
        $(CURRENT_PAGE.comments_sel).remove();
    }

    if (CURRENT_PAGE.extra) {
        CURRENT_PAGE.extra();
    }

     // Sidebar follows
	if ($("#sidebar").length && !$("#image").length) {
		$("#sidebar").css({
			'position': 'fixed',
			'top'	: "125px"
		});
	}

	 if ($("#image").exists()) {
        var el = $("#image-resize-link, #highres-show");
        if (el.exists()) {
            var href = el.attr("href");
            $("#image-resize-notice, #resized_notice").remove();

            var img = new Image();
            img.onload = function() {
                var $parent = $("#image").parent().css("text-align", "center");
                $(window).resize(function(){
                    var height = Math.round($(window).height() - $parent.offset().top);
                    var width = Math.round($parent.width());

                    // Fit the image
                    var aspectRatio = img.width / img.height;
                    if (aspectRatio / (width / height)) {
                        // More portrait than space provided
                        $("#image").width(aspectRatio* height).height("auto");
                    } else {
                        // More landscape than space provided
                        $("#image").width("100%").height(width / aspectRatio);
                    }
                    $("#image").attr("src", el.attr("href")).show().height("auto");
                }).resize();

                $("#image").click(function(){
                    $(this).toggleClass("zoom");
                });
            }

            img.src = el.attr("href");
            $("#image").hide();
        }
    }
}

BRIDGE.css({
    "img#image.zoom": {
        width: "auto !important",
    },
});
















