﻿// ==UserScript==
// @name        akiba
// @namespace   none
// @include     http://www.akiba-online.com/forums/*
// @version     2015.1.11
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/6268a10cec52bfbaf4a8f36a14cfe3ab36e69bfd/akiba/akiba.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/6268a10cec52bfbaf4a8f36a14cfe3ab36e69bfd/akiba/akiba.user.js
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var IMAGE = {
    put: function(url, imgArr) {
        sessionStorage.setItem(url, imgArr.join(","));
    },
    get: function(url) {
        var items = sessionStorage.getItem(url);
        return items ? items.split(",") : [];
    }
};

function startThreadImages() {
    var trs = $("#content div.discussionList li");
    
    for (var i = 0; i < trs.length; i++) {
        var aEl = trs.eq(i).find("a.PreviewTooltip");
        loadImages(aEl);
    }
}

function loadImages(aEl) {
    aEl.attr("target", "_blank");
    var parent = aEl.parent().parent();
    var url = "http://www.akiba-online.com/" + aEl.attr("href");
    
    function putToPage(imgUrls) {
        parent.append("<br/>");
        for (var i = 0; i < Math.min(imgs.length, 5); i++) {
            parent.append(
                $("<a>").attr("href", imgs[i]).attr("target", "_blank").append(
                    $("<img>").attr({"src": imgs[i], alt: ""}).addClass("prv")
                )
            );
        }
    }

    // Get images
    var imgs = IMAGE.get(url);
    if (imgs.length) {
        putToPage(imgs);
    } else {
        SimpleHtmlParser.get(url, function(parser){
            var sectionText = parser.html.between("slot: message_user_info_text", "ROTATING_ADS_MESSAGE_BELOW_FIRST");
            parser = new SimpleHtmlParser(sectionText);
            var inf = 0;
            while(parser.containsTextThenSkip('class="bbCodeImage LbImage"')) {
                if (inf++ > 1000){
                    alert("infinite loop")
                    return;
                }
                imgs.push(parser.getAttributeInCurrentTag("src"));
                parser.containsTextThenSkip("/>");
            }
            // Attach the images to page
            if (imgs.length) {
                IMAGE.put(url, imgs);
                putToPage(imgs);
            }
        });
    }
}

var here = window.location.href;
if (here.contains("attachmentshow.php?")) { 
    window.location.href = here.replace("attachmentshow.php", "attachment.php");
} else {
    startThreadImages();
}



BRIDGE.css({
    "img.prv": {
        height: "300px"
    },
    "div.listBlock.posterAvatar": {
        display: "none"
    }
});