﻿// ==UserScript==
// @name        zbeasy
// @namespace   matthewn4444
// @version     2014.12.20
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/zbeasy/zbeasy.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/zbeasy/zbeasy.user.js
// @include     *zbbit.com/*
// @require     http://code.jquery.com/jquery-1.8.0.min.js
// @require     https://jobmine-plus.googlecode.com/svn/trunk/resource/scripts/bridgeAPI.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var ImgServ = [
    {
        urlContains: "picth.com",
        execute: function(url, done) {
            get(url, function(data){
                var i = data.indexOf("normal smallbox");
                if (i == -1) return done();
                var imgSrc = data.between("<a href='", "'", i);
                done(imgSrc && imgSrc != "" ? imgSrc : null);
            });
        },
        fromThumb: function(url, done) {
            done(url.replace("/thumb/", "/get/"));
        },
    },
    {
        urlContains: "oopzeed.com",
        execute: function(url, done) {
            done(url.replace("views", "upload/big") + ".jpg");
        },
    },
    {
        urlContains: "downslam.com",
        execute: function(url, done) {
            get(url, function(data){
                var i = data.indexOf("alt");
                if (i==-1)return done();
                i = data.lastIndexOf("<img", i);
                var imgSrc = data.between('src="', '"', i);
                done(imgSrc && imgSrc != "" ? imgSrc : null);
            });
        },
    },
];

String.prototype.contains = function(str) {
    return this.indexOf(str) != -1;
}

String.prototype.between = function(beginStr, endStr, index) {
	index = index || 0;
	var start, end;
	if ((start = this.indexOf(beginStr, index)) === -1) return null;
	start += beginStr.length;
	if ((end = this.indexOf(endStr, start)) === -1) return null;
	return this.substring(start, end);
}
function appendCSS(cssObj) {
    var cssString = "";
    for(var selector in cssObj) {
    var eachSelector = cssObj[selector];
        var propString = "";
        for(var property in eachSelector) {
            propString += property + ":" + eachSelector[property] + ";";
        }
        cssString += selector + "{" + propString + "}";
    }
    $("body").append("<style>" + cssString + "</style>");
    cssString = null;
}

function get(location, callback) {
    GM_xmlhttpRequest({
        method: "GET",
        url: location,
        onload: function(res) {
            if (res.readyState == 4 && res.status == 200) {
                callback(res.responseText);
            }
        }
    });
}

function getImage(src, callback) {
    // Search for image service
    for (var i = 0; i < ImgServ.length; i++) {
        var service = ImgServ[i];
        if (src.contains(service.urlContains)) {
            service.execute(src, function(imageUrl){
                if (imageUrl) {
                    a.after("<a href='" + imageUrl + 
                        "' target='_blank'><img style='width:100%;margin:10px 0;' \
                        src='" + imageUrl + "'/></a>");
                }
            }.bind(this));
            return;
        }
    }
    callback(); // Failed
}

function parseImageUrl(url) {
    var img;
    if (url.contains("www")) {
        img = "http://" + url.substring(url.indexOf("www"));
    }
    return unescape(img);
}

var url = window.location.href;

// Clean up
$("a[rel='nofollow']").eq(0).parents("table").eq(0).remove();
$("a[href='my.php']:last").parents("table").eq(0).remove();
$("marquee").remove();
$(".mainouter:last").remove();
$("a[href*='send2love']").eq(0).parents("table").eq(0).remove();
$("a[href*='uTorrent']").parent().prev().andSelf().remove();

if (url.contains("details.php")) {
    // Details
    var td = $("td.rowhead:first").parent().next().next().next();
        a = td.find("a"),
        img = a.attr("href");
    if (img) {
        getImage(parseImageUrl(img), function(imgSrc){
            if (imgSrc) {
                a.html("<img width='800' src='" + imgSrc + "'/>");
            }
        });
    }

    td.next().find("img").each(function(){
        var o = $(this),
            src = o.attr("src");
            
        if (!src) { // a tag
            src = o.attr("href");
            o.attr("target", "_blank");
        } else {    // Image    
            o.parent().attr("target", "_blank");
        }

        // Search for image service
        if (src) {
            src = parseImageUrl(src);
            for (var i = 0; i < ImgServ.length; i++) {
                var service = ImgServ[i];
                if (src.contains(service.urlContains)) {
                    service.fromThumb(src, function(imageUrl){
                        if (imageUrl) {
                            o.parent().attr("href", imageUrl);
                        }
                    }.bind(this));
                    return;
                }
            }
        }
    });
} else if (url.contains("browse.php")) {
    // Set the categories
    $("a.catlink").each(function(){
        var o = $(this);
        var categories = ["Anime", "Games", "Sport", "Mobile", "Programs", "Books", 
                        "Docs, News", "Image", "Comedy, Talk show", "Clips", "DVDs",
                        "VCDs", "TV", "Tai Music", null, null, null, "Other"];
        var category = parseInt(o.attr("href").substring(15)) - 1;
        if (categories.hasOwnProperty(category) && categories[category]) {
            o.text(categories[category]);
        }
    });
}

appendCSS({
    '.mainouter': {
        'margin-bottom': '40px',
    },
});
