﻿// ==UserScript==
// @name        Webm
// @namespace   none
// @version     2015.12.2
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/Webm/Webm.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/Webm/Webm.user.js
// @include     *.webm
// @include     *webmup.com/*
// @include     *gfycat.com/*
// @include     *catbox.moe/*
// @include     *my.mixtape.moe/*
// @include     *a.pomf.se/*
// @include     *pomf.nightboy.net*
// @include     *webmshare.com/*
// @include     *d.maxfile.ro/*
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       none
// ==/UserScript==

function doIt(n) {
    var v = $("video");
    console.log(v.length)
    if (v.length) {
        if (window.location.href.endsWith(".webm") || window.location.href.endsWith(".mp4")
                || window.location.href.contains("webmshare") || window.location.href.contains("gfycat")) {
            $(document).ready(function(){
                v.css({margin:0,transform:'none',top:0,right:"0 !important",left:"0 !important",bottom:"0 !important",position:'fixed','z-index':1000,background:"black"}).attr({loop:"", autoplay:""});
            });
            v.parents().css({"overflow": "hidden", "padding": 0, "margin": 0});
            setTimeout(function() {
                $("video").css({width:"100%", height:"100%", "max-width": 'none'}).attr({loop:"", autoplay:""});
            }, 1000);
        } else {
            var src = v.find("source").eq(0).attr("src");
            if (src) {
                window.location.href = src;
            }
        }
        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', function(){
            v.css({width:"100%", height:"100%"}).attr({loop:"", autoplay:""});
        });
    }
    if (n < 8) {
        setTimeout(doIt.bind(null, n + 1), 200);
    }
}
doIt(0);

BRIDGE.css({
    "div": {
        "transform" : "none  !important"
    },
    ".share-video video, video": {
        "position": "fixed !important",
        "left": "0 !important",
        "right": "0 !important",
        "top": "0 !important",
        "bottom": "0 !important",
    }
})