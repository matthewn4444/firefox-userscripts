// ==UserScript==
// @name        420chn to Images
// @namespace   none
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/420chn_to_Images/420chn_to_Images.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/420chn_to_Images/420chn_to_Images.user.js
// @include     http://boards.420chan.org/h/res/*
// @include     https://boards.420chan.org/h/res/*
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @version     2016.1.1
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var urls = [];
$("#delform img.thumb").each(function() {
    urls.push($(this).attr("src"));
});
$("#content, #foot, #infobar").remove();

var $content = $("<div>").attr("id", "layout")
$(document.body).append($content);
for (var i = 0; i < urls.length; i++) {
    $content.append(
        $("<a>").attr({"href": urls[i].replace("/thumb/", "/src/").replace(/s\.jpg$/, ".jpg"), "target": "_blank"})
            .append($("<img>").attr("src", urls[i]))
    );
}

BRIDGE.css({
    "#layout": {
        "text-align": "center",
    },
    "#layout a": {
        display: "inline-block",
        "box-sizing": "border-box",
        margin: "20px",
        "vertical-align": "top",
    },
    "#layout img": {
        width: "250px",
    },
    ".single-column": {
        
    },
});

//$("#layout").layout({
//    itemMargin : 30,
//    itemPadding : 30
//});
