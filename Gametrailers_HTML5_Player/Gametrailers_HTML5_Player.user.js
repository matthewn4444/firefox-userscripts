﻿// ==UserScript==
// @name        Gametrailers HTML5 Player
// @namespace   mattng
// @version     2014.12.20
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/c4f0d30bb9633dcb8ffca0f64fd9d7c04b7eb43a/Gametrailers_HTML5_Player/Gametrailers_HTML5_Player.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/c4f0d30bb9633dcb8ffca0f64fd9d7c04b7eb43a/Gametrailers_HTML5_Player/Gametrailers_HTML5_Player.user.js
// @icon        https://lh5.googleusercontent.com/-pmi2cPz9Gcg/AAAAAAAAAAI/AAAAAAAAsnE/WXJzM-gXYpE/s120-c/photo.jpg
// @include     http://www.gametrailers.com/*
// @grant       none
// ==/UserScript==

var VIDEO_PLAYER = "player_wrap";

/*=======================*\
        FUNCTIONS
\*=======================*/
function byId(id, parent/*optional*/) {
    if (!parent) parent = document;
    return parent.getElementById(id);
}

function byClass(className, parent/*optional*/) {
    if (!parent) parent = document;
    return parent.getElementsByClassName(className);
}

function byTag(tag, parent/*optional*/) {
    if (!parent) parent = document;
    return parent.getElementsByTagName(tag);
}

function createEl(elementName, id /*optional*/, attrArr /*optional*/, parentEl /*optional*/) {
    var el = document.createElement(elementName);
    if (id) {el.id = id;}
    if (attrArr) {
        for (var attr in attrArr) {
            el.setAttribute(attr, attrArr[attr]);
        }
    }
    if (parentEl) {
        parentEl.appendChild(el);
    }
    return el;
}

function createText(txt, parentObj) {
	var node = document.createTextNode(txt);
	if (parentObj) {
		parentNode.appendChild(node);
	}
    return node;
}

function insertBefore(el, elAfter) {
    if (el && elAfter && elAfter.parentNode) {
        elAfter.parentNode.insertBefore(el, elAfter);
    }
}

function addJS(code) {
    if(typeof(code) == "function") {  
        //Strips the function(){} wrapper
        code =  (code+"").replace(/\n/g, "");
        code = code.replace(/^[^{]*?{/, "");
        code = code.substring(0, code.lastIndexOf("}"));
        var script = document.createElement("script");
        script.innerHTML = code;
        document.head.appendChild(script)
    }
}

function ajaxJsonGet(url, callback, error) {
    if (!error) error = function(){};
    var httpRequest;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE 8 and older
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var response = JSON.parse(httpRequest.responseText);
                callback(response);
            } else {
                log("Problem with getting video");
                error();
            }
        }
    };
    httpRequest.open('GET', url, true);
    httpRequest.send(null);
}

/*=======================*\
       VIDEO PLAYER
\*=======================*/
function shouldAutoPlay() {
    return window.localStorage &&
            (window.localStorage.hasOwnProperty("video-player-autoplay") 
            && window.localStorage.getItem("video-player-autoplay") === "true");
}

function loadOriginalPlayer() {
    addJS(function(){
		var VIDEO_PLAYER = "player_wrap";
        $Crabapple.Player.players[VIDEO_PLAYER] = new window.oldPlayerClass(
            window.passedObj.a, window.passedObj.b, window.passedObj.c
        );
        var playerWrapper = document.getElementById(VIDEO_PLAYER);
        playerWrapper.style.display = "block";
        var objects = getElementsByTagName("object", playerWrapper);
        if (objects && objects[0]) {
			objects[0].style.display = "block";
        }
    });    
}

function getVideoUrl(callback, errorfn) {
	errorfn = errorfn || function(){};
	var downloadBtn = byClass("download_button", byId('mod_j8j95m'));
    if(downloadBtn && downloadBtn.length > 0) {
		downloadBtn = downloadBtn[0];
		var playerWrapper = byId(VIDEO_PLAYER);
		
		var token = downloadBtn.getAttribute("data-token"),
            videoId = downloadBtn.getAttribute("data-video");
        if (videoId) {
            ajaxJsonGet('/feeds/video_download/' + videoId + '/' + token, 
                function(responseObj){
					var url = responseObj.url;
					if (url) {
						changeDownloadButton(downloadBtn, url);
						callback(url);
                    } else {
						alert("Cannot get video");
                    }
                },
                errorfn      // On error
            );
            return true;
        }
	}
	return false;
}

function changeDownloadButton(downloadBtnEl, url) {
    downloadBtnEl.style.display = "none";
    var link = createEl("a", null, {
        "href"  : url,
        "class" : "download_button",
    }, downloadBtnEl.parentNode);
}

function setupAutoPlay() {
    if (!window.localStorage) { // Does it support this?
        return;
    }
    // HTML & Styling
    var thumbsPanel = byClass("utils_content_rating", byId("t2_lc"))[0];
    console.log(thumbsPanel)
    var container = createEl("div", null, {
        "style" :  "background: #EDEDED;border:1px solid #CCC;border-bottom:none;\
                    color:#888;font-weight:bold;font-family:Arial;font-weight:bold;\
                    padding:10px 0;font-size:10px;user-select:none;\
                    -moz-user-select:none",
    });
    var chkbx = createEl("input", "auto-play-chkbx", {
        "type"      : "checkbox",
        "style"     : "margin: 0 10px",
    }, container);
    chkbx.checked = shouldAutoPlay();
    var label = createEl("label", null, {
        "for"       : "auto-play-chkbx",
    }, container)
    label.appendChild(createText("AUTOPLAY"));
    insertBefore(container, thumbsPanel);
    byTag("p", byClass("video_information-player")[0])[0].style.minHeight = "130px";
    
    chkbx.addEventListener("click", function(e){
        window.localStorage.setItem("video-player-autoplay", chkbx.checked);
    }, false);
}

function dontLoadFlashPlayer() {
    addJS(function(){
        window.oldPlayerClass = MTVNPlayer.Player;
        window.passedObj = {};
        MTVNPlayer.Player = function(a, b, c){
            window.passedObj.a = a;
            window.passedObj.b = b;
            window.passedObj.c = c;
        }
    });
    var player = byTag("object", byId(VIDEO_PLAYER));
    if (!player.length) return alert("Cannot find player");
    player[0].style.display = "none";
}

/*=======================*\
           BEGIN
\*=======================*/
function begin() {
	setupAutoPlay();
	if (getVideoUrl(function(url){ 	// Async Success
		var player = byTag("object", byId(VIDEO_PLAYER));
		if (player.length) player = player[0];
		//var width = parseInt(player.getAttribute("width"), 10);
		//var height = parseInt(player.getAttribute("height"), 10);
		
		// Load the HTML5 Video Player
		var prop = {
			height: "100%",
			width: "100%",
			controls: "controls",
		};
		if (shouldAutoPlay()) prop["autoplay"] = "true";
		var vidEl = createEl("video", "html5-player", prop, player.parentNode);
		var type = null;
		if (url.indexOf(".mp4?") != -1) {
			type = "mp4";
		} else {
			alert("Cannot play this format, download it.\n " + url);
		}
		createEl("source", null, {src:url, type:"video/" + type}, vidEl);
		createText("Your browser does not support MP4 video, please upgrade.", vidEl);
		
	}, 	// Async Failure
		loadOriginalPlayer
	)) {	// The next few lines runs before the upper
		dontLoadFlashPlayer();
	}
}

if (byId(VIDEO_PLAYER)) {
    begin();
}