// ==UserScript==
// @name        Wallpaper
// @namespace   none
// @include     http://*
// @include     https://*
// @exclude     *www.google.com/maps*
// @exclude     *.php?attachmentid=*
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/390b72b942a14674281a5559352bdadc0e603866/Wallpaper/Wallpaper.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/390b72b942a14674281a5559352bdadc0e603866/Wallpaper/Wallpaper.user.js
// @version     2016.1.1
// @grant       GM.xmlHttpRequest
// ==/UserScript==
var host = "http://localhost:1111";

// Constants
var density = window.devicePixelRatio;
var displayWidth = window.screen.width;
var displayHeight = window.screen.height;
var displayRatio = displayWidth / displayHeight;

// Global Variables
var pollInterval;
var currentId;
var failed = false;
var isFocus = false;
var isInside = true;
var lastTime = Date.now();
var images = [];
var values = [];
var isOnMain = window.screen.left == 0 && window.screen.top == 0;		// TODO doesnt work if 2nd+ monitor is higher than first

// Hacky variables that should be calculated by Addon
var windowToolbarsHeight = /*window.outerHeight - window.innerHeight + 1;*/ 35;
var windowOffsetSize = 5;

//function wallpaperPeek(flag) {
//    var $obj = $("#wallpaper-background-wrapper").siblings().stop();
//    if (flag) {
//        $obj.fadeOut(300);
//    } else {
//    console.log("fade in")
//        $obj.fadeIn(400);
//    }
//}

function start() {
    // Poll the url
    beginPoll(500);

    // Inject background wallpaper elements on page
    $("body").append(
        $("<div>").attr("id", "wallpaper-background-wrapper").append(
            $("<div>").attr("id", "wallpaper-background-1").addClass("wallpaper-background").hide()
        ).append(
            $("<div>").attr("id", "wallpaper-background-2").addClass("wallpaper-background").hide()
        )/*.append(
            $("<div>").attr("id", "wallpaper-background-peek")
        )*/
    );

    $(window).on("resize", function() {
        if ($("#wallpaper-background-1").is(":visible")) {
            adjustImage(0);
        } else {
            adjustImage(1);
        }
    }).on("blur", function() {
        isFocus = false;
        beginPoll(2000);
    }).on("focus", function() {
        isFocus = true;
        beginPoll(500);
    });
    $(document).on("mouseleave", function() {
        isInside = false;
        updatePosition();
    }).on("mouseenter", function() {
        isInside = true;
        //wallpaperPeek(false);
    });

    //$("#wallpaper-background-peek").on("mouseenter", function() {
    //    setTimeout(function() {
    //        if (isInside) {
    //            wallpaperPeek(true);
    //        }
    //    }, 10)
    //}).on("mouseleave", function() {
    //    setTimeout(function() {
    //        if (isInside) {
    //            wallpaperPeek(false);
    //        }
    //    }, 10)
    //});
}

function updatePosition() {
    if (isFocus || !isInside) {
        var now = Date.now();
        if (now - lastTime > 10) {
            lastTime = now;
            if ($("#wallpaper-background-1").is(":visible")) {
                adjustImage(0);
            } else {
                adjustImage(1);
            }
        }
        requestAnimationFrame(updatePosition);
    }
}

var lastScreenX;
var lastScreenY;


// TODO if using multiple size monitors, this will fail
function adjustImage(num, isNew) {
    if (!values.length) return;
    var $obj = $("#wallpaper-background-" + (num + 1));
    var x = window.screen.left - window.screenX;
    var y = window.screen.top - window.screenY;

    if (x == lastScreenX && y == lastScreenY && !isNew) {
      return;
    }

  	// TODO def not scalable more than 2 monitors
  	if (window.screen.left > 0 && window.screen.top == 0) {
      	if (isOnMain) {
         		$obj.css("background-image", "url(" + images[1].src + ")");
        }
     		isOnMain = false;
    } else {
      	if (!isOnMain) {
          	$obj.css("background-image", "url(" + images[0].src + ")");
        }
      	isOnMain = true;
    }

  	lastScreenX = x;
  	lastScreenY = y;


    var offsetTop = 0;
    var offsetLeft = 0;

    if (!isNew && $obj.data("image-offset-left")) {
        offsetLeft = $obj.data("image-offset-left");
        offsetTop = $obj.data("image-offset-top");
    } else {
        var ratio = (values[num].width / values[num].height);
        var width = window.screen.width;
        var height = window.screen.height;

        // Scale the image to screen size to "fit" wallpaper like background
        if (ratio < displayRatio) {
            // Portrait, scale by width
            height = Math.round(width / ratio);
            offsetTop = Math.round(-(height - window.screen.height) / (2 * density));
        } else {
            // Landscape, scale by height
            width = Math.round(height * ratio);
            offsetLeft = Math.round(-(width - window.screen.width) / (2 * density));
        }

        offsetTop -= Math.round(windowToolbarsHeight);
        offsetLeft -= Math.round(windowOffsetSize / density);
        $obj.data("image-offset-left", offsetLeft);
        $obj.data("image-offset-top", offsetTop);

        $obj.css({
            "width": width + "px",
            "height": height + "px",
            "background-image": "url(" + values[num].src + ")",
            "background-size": width + "px " + height + "px"
        });
    }
    var l = x + offsetLeft;
    var t = y + offsetTop;

    $obj.css({
        top: t,
        left: l,
    })
}

function showImage(img) {
    var newImage = img.src;
    var iw = img.width;
    var ih = img.height;
  	var change = isOnMain && img.src.includes("image0") || !isOnMain && img.src.includes("image1");		// TODO not scalable more than 2 monitors

  console.log(change, isOnMain, img.src)

    if ($("#wallpaper-background-1").is(":visible")) {
      	if (!change) {
          	values[0] = {
                src: newImage,
                width: iw,
                height: ih
            };
          	return;
        }
      	values[1] = {
        		src: newImage,
        		width: iw,
        		height: ih
        };
	      $("#wallpaper-background-2").css("z-index", "-1").fadeIn(800, function() {
  		      $("#wallpaper-background-1").hide();
        });
      	$("#wallpaper-background-1").css("z-index", "-2");
      	adjustImage(1, true);
    } else {
      	if (!change && $("#wallpaper-background-1").is(":visible")) {
          	values[1] = {
                src: newImage,
                width: iw,
                height: ih
            };
          	return;
        }
        values[0] = {
            src: newImage,
            width: iw,
            height: ih
        };
      	$("#wallpaper-background-1").css("z-index", "-1");
      	if (values.length > 1) {
          	$("#wallpaper-background-1").fadeIn(800, function() {
              	$("#wallpaper-background-2").hide();
            });
        } else {
          	$("#wallpaper-background-1").show();
        }
      	$("#wallpaper-background-2").css("z-index", "-2");
      	adjustImage(0, true);
    }
}

function beginPoll(timeout) {
    updatePosition();
    clearPoll();
    pollInterval = setInterval(poll, timeout);
    poll();
}

function clearPoll() {
    if (pollInterval) {
        clearInterval(pollInterval);
        pollInterval = null;
    }
}

function poll() {
    get(host + "/poll", function(d) {
      	var id = d.id;
      	if (typeof d.changed === "undefined") {
          	console.log("Not ready yet");
          	return;
        }
      	var url = d.url + d.changed + "?i=" + id;
        if (currentId != id) {
          	currentId = id;
            var image = images[d.changed] = new Image();
            image.onload = function() {
                console.log(image)
                showImage(image);
            }
            image.src = url;
        }
    }, function() {
        // Failure
        failed = true;
        console.log("Fail to reach server. Run it and then refresh page");
        clearPoll();
    });
}

function get(url, callback, errorCallback) {
    GM.xmlHttpRequest({
        method:"GET",
        url: url,
        overrideMimeType: 'text/plain; charset=x-user-defined',
        onload: function(response) {
            if (response && response.status === 200) {
                callback(JSON.parse(response.responseText));
            } else {
                console.log("dsds");
            }
        },
        onerror: errorCallback
    });
}

function preloadImage(i) {
  	return new Promise((res, rej) => {
      	var url = host + "/image" + i;
      console.log(url)
      	var image = images[i] = new Image();
      	image.onload = res;
      	image.onerror = rej;
      	image.src = url;
    });
}

if (window.top == window.self) {
    BRIDGE.css({
        "#wallpaper-background-wrapper, .wallpaper-background": {
            "position": "fixed",
            "top": 0,
            "left": 0,
            "z-index": "-2",
            "background-repeat": "no-repeat"
        },
        "#wallpaper-background-peek": {
            "position": "fixed",
            "right": "0",
            "bottom": "0",
            "width": 0,
            "height": 0,
            "border-style": "solid",
            "border-width": "0 0 60px 60px",
            "border-color": "transparent transparent rgba(0,118,255,0.5) transparent",
        }
    });
  	get(host + "/number", function(d) {
      	if (d == 0) {
          	return alert("failed wallpaper");
        }

				// Preload wallpaper data per monitor
      	var req = [];
      	for (var i = 0; i < Math.min(d.num, 2); i++) {
          	req.push(preloadImage(i));
        }

      	Promise.all(req)
      			.then(start)
      			.catch((e) => console.error(e))

    });

    //start();
}