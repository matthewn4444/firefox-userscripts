// ==UserScript==
// @name        SanKKCmplx
// @namespace   none
// @version     2015.12.2
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/SanKKCmplx/SanKKCmplx.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/SanKKCmplx/SanKKCmplx.user.js
// @include     https://chan.sankakucomplex.com/
// @include     https://idol.sankakucomplex.com/
// @include     https://chan.sankakucomplex.com/post/*
// @include     https://idol.sankakucomplex.com/post/*
// @include     https://chan.sankakucomplex.com/?tags*
// @include     https://idol.sankakucomplex.com/?tags*
// @include     https://chan.sankakucomplex.com/?next*
// @include     https://idol.sankakucomplex.com/?next*
// @include     https://chan.sankakucomplex.com/user/*
// @include     https://idol.sankakucomplex.com/user/*
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       unsafeWindow
// ==/UserScript==


var user = "someguy444"
var pass = "pokemon"

//unsafeWindow.eval = function() {
//    alert("no")
//}

//unsafeWindow.setTimeout = function() {
//    alert("no1")
//}

//unsafeWindow.setInterval = function() {
//    alert("no2")
//}

try{

  /*
// Attempt login
if (window.location.href.includes("/user/login")) {
    $("#user_name").val(user);
    $("#user_password").val(pass);
    $("input[name='commit']").click();
} else if ($("#navbar a:first[href='/user/login']").length) {
    window.location.href = "http://"+ window.location.host + "/user/login";
} else if (window.location.href.includes("user/home")) {
    window.location.href = "https://" + window.location.host;
}

// Toggle the Autopaging off
$(window).load(function() {
if ($("#sc-auto-toggle span:contains('On')").length) {
    if (!$("div.post-content-notification:contains('No matching posts')").length) {
        if ($("#navbar a:contains('Login')").length) {
            $("#sc-auto-toggle span:contains('On')").click();
            window.location.reload();
        }
    }
}
});
  */

// Make the image high res
if ($("#image-link").length) {
    var $el = $("#image-link");
	var url = $el.attr("href");
	//$("#resized_notice").remove();
	$el.attr("href", "#");
	if (url)  {
		$("#image").attr("src", url)
            .removeAttr("width")
            .removeAttr("height")
            .css("width", "95%");
	}
}

$("#post-content").css("padding-top", 0)

// Fix original image
$("#highres").attr("target", "_blank");

// Fixed their javascript temp
function fixImgs() {
    var els =document.evaluate( '//span[@class="thumb"]',
            document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null );
    if (els.snapshotLength>0) {
        for(var i = 0; i < els.snapshotLength; i++) {
            var el = els.snapshotItem(i);
            var child = el.firstChild;
            el.className = "thumb done";
            child.setAttribute("target", "_blank");
            child.removeAttribute("onclick");
        }
    }
}
window.onscroll = fixImgs;
fixImgs();
  
  
var start = Date.now();
function hideAds() {
  	if ($("#sank-prestitial").length) {
      	$("#sank-prestitial").remove();
      	$(document.body).removeClass("no-scroll")
    } else if(Date.now() - start > 5000) {
      	setTimeout(hideAds, 500);
    }
}
setTimeout(hideAds, 500);
    

}catch(e){alert(e)}

