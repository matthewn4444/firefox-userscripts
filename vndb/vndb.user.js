﻿// ==UserScript==
// @name        vndb
// @namespace   matthewn4444
// @version     2015.12.02
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/vndb/vndb.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/vndb/vndb.user.js
// @include     http://vndb.org/*
// @include     https://vndb.org/*
// @require     http://matthewng.herokuapp.com/userscripts/all.js
// @grant       none
// ==/UserScript==

String.prototype.rbetween = function(begin, end, index) {
    index = index || this.length - 1;
    var sIndex = this.lastIndexOf(begin, index);
    if (sIndex == -1) return null;
    sIndex += begin.length;
    var eIndex = this.indexOf(end, sIndex);
    if (eIndex == -1) return null;
    return this.substring(sIndex, eIndex);
}

var item = $("#maincontent div.mainbox.browse.vnbrowse table.stripe");
// =========================================
// Detect resolution and add a column for it
// =========================================
if (item.length) {
    var i = 0;

    // Add the headers (Resolution and Link)
    item.find("thead td:contains('Title')")
        .after("<td class='tc_r'>Resolution</td><td class='tc_l'>Link</td>");
    var queue = item.find("tbody td a").attr("target", "_blank");

    // As a queue, go each row and add the resolution and external link
    function getResolution() {
        if (i >= queue.length) {
            return;
        }
        var a = queue.eq(i);
        var href = a.attr("href");
        if (href) {
            // If the image is cached in session, then we will just attach it
            if (localStorage.getItem(href)) {
                var data = localStorage.getItem(href).split("|");
                // Handle link
                if (data.length > 2) {
                    a.parent().after("<td class='tc_l'><a href='" + data[2] + "' target='_blank'>Link</td>");
                } else {
                    a.parent().after("<td class='tc_l'>None</td>");
                }
                a.after("<br/><a target='_blank' href='" + a.attr("href") + "'><img src='" + data[1] + "'/>");
                a.parent().after(data[0]);
                i++;
                getResolution();
                return;
            }
            $.get(href, function(r){
                // Get the image
                var index = r.indexOf("vnimg");
                var imgUrl = null;
                if (index != -1) {
                    var img = r.between('src="', '"', index);
                    if (img.indexOf(".jpg") !== -1) {
                        imgUrl = img;
                    } else {
                        console.log("Cannot find image")
                    }
                } else {
                    alert("Cannot find image [1]");
                }
                var url = r.rbetween('href="/r', '"');
                if (url && url != "") {
                    $.get("/r" + url, function(res){
                        var index = res.indexOf("Resolution");
                        var resolution = null;
                        var text = null, link = null;

                        function setResolution(res) {
                            var s = res.split("x");
                            var isWide = 1.7 <= parseInt(s[0], 10) / parseInt(s[1], 10);
                            if (isWide) {
                                text = "<td class='tc_r' style='color:green;'>Widescreen [" + res + "]</td>";
                            } else {
                                text = "<td class='tc_r'>Standard [" + res + "]</td>";
                            }
                        }

                        // Get resolution
                        if (index !== -1) {
                            resolution = res.between("<td>", "</td>", index);
                            if (resolution) {
                                setResolution(resolution);
                            }
                        } else {
                            // Try to find the image in the older page's screenshots
                            index = res.indexOf(":scr");
                            if (index == -1) {
                                text = "<td class='tc_r' style='color:red;'>Unknown</td>";
                            } else {
                                var start = res.lastIndexOf(":", index - 1) + 1;
                                resolution = res.substring(start, index);
                                setResolution(resolution);
                            }
                        }

                        // Get link
                        index = res.indexOf("Links");
                        if (index != -1) {
                            link = res.between('href="', '"', index);
                        }
                        i++;
                        if (link) {
                            if (imgUrl)
                                localStorage.setItem(href, text + "|" + imgUrl + "|" + link);
                            a.parent().after("<td class='tc_l'><a href='" + link + "' target='_blank'>Link</td>");
                        } else {
                            if (imgUrl)
                                localStorage.setItem(href, text + "|" + imgUrl);
                            a.parent().after("<td class='tc_l'>None</td>");
                        }
                        if (imgUrl) {
                            a.after("<br/><a target='_blank' href='" + a.attr("href") + "'><img src='" + imgUrl + "'/>");
                        } else {
                            a.after("<br/><a target='_blank' href='" + a.attr("href") + "'>No Image");
                        }
                        a.parent().after(text);
                        getResolution();
                    });
                }
            });
        }
    }
    if (queue.length)
        getResolution();
}

// =================
// Show NSFW content
// =================
item = $("#screenshots");
if (item.length || $("#nsfw_hid").length) {
    // The cover art
    $("#nsfw_hid").show();
    $("#nsfw_show, #iv_view, #ivimgload").hide();

    $(".hidden").removeClass("hidden");

    // Some clean up and rearranging
    var firstBox = $("#maincontent > .mainbox:first");
    firstBox.after(
        $("<div>").addClass("mainbox").append(
            $("<h1>").text("Tags")
        ).append(
            $("<div>").addClass("clearfloat").css("height", "5px")
        ).append($("#tagops")).append($("#vntags").css("border", "none"))
    )

    $("#maincontent > .mainbox:first").after(item);

    // Show as large images
    var imgs = item.find("img").each(function(){
        var a = $(this).parent(),
            src = a.attr("href"),
            thmb = $(this).attr("src");
        try {
            var res = a.attr("data-iv").split(":")[1].split("x"),
                width = parseInt(res[0], 10),
                height = parseInt(res[1], 10);
            $(this).removeAttr("width height").attr("src", src)
                .css({"background": "no-repeat contains url('" + thmb + "')", "border": "none"});
            a.on("click", function(){
                window.open(src)
            }).css("margin", 0);
        } catch(e) {alert(e);}
    });
    $("#screenshots > .scr:first").append($("#screenshots > .scr:gt(0) a.scrlnk"));
    $("#screenshots > .scr:first").prepend($("#screenshots a.scrlnk.nsfw"));
    $(window).resize(function(){
        $("body").toggleClass("small", $("#screenshots").width() < 700);
    }).resize();
}

BRIDGE.css({
    ".mainbox .tc1 img": {
        height: "200px"
    },
    ".mainbox .scrlnk img": {
        "max-width": "50%"
    },
    "body.small .mainbox .scrlnk img": {
        "max-width": "100%"
    },
});











