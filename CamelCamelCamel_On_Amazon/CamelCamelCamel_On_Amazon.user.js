// ==UserScript==
// @name        CamelCamelCamel On Amazon
// @namespace   none
// @include     http://www.amazon.com/*
// @version     2015.7.11
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM_xmlhttpRequest
// @grant       GM_openInTab
// @grant       unsafeWindow
// ==/UserScript==

if ($("#priceblock_ourprice").length) {
    var id = null;
    var e = unsafeWindow.ue_url.indexOf("/ref=");
    var s = unsafeWindow.ue_url.lastIndexOf("/", e - 1);
    if (e == -1 || s == -1) {
        // Try after /dp/
        if (unsafeWindow.ue_url.indexOf("/dp/") != -1) {
            id = unsafeWindow.ue_url.between("/dp/", "/");
        } else {
            alert("Failed");
        }
    } else {
        id = unsafeWindow.ue_url.substring(s + 1, e);
    }
    if (id) {
        $("#price").append(
            $("<button>")
                .text("Check CamelCamelCamel")
                .css("margin-top", "-25px")
                .click(function(){
                    GM_openInTab("http://camelcamelcamel.com/product/" + id)
                })
        );
    }
}
