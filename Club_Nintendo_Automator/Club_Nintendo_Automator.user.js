﻿// ==UserScript==
// @name        Club Nintendo Automator
// @namespace   none
// @include     https://cnsurvey.nintendo.com/s?s=*
// @version     2015.2.17
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/ac400d851f3ac42652e1a3c5533fc395259aeda5/Club_Nintendo_Automator/Club_Nintendo_Automator.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/ac400d851f3ac42652e1a3c5533fc395259aeda5/Club_Nintendo_Automator/Club_Nintendo_Automator.user.js
// @icon        http://images.hngn.com/data/images/full/58438/club-nintendo-logo.jpg
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       none
// ==/UserScript==

$(window).ready(function() {
$("td.question").each(function(){
    $(this).find("input:first").prop("checked", true);
    $(this).find("input.numericInput").val(1);
    $(this).find("textarea").val("N/A");
});
});