// ==UserScript==
// @name        Getchu
// @namespace   none
// @include     http://www.getchu.com/php/attestation.html?*
// @include     http://www.getchu.com/soft_sampleimage.phtml?id=*
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/Getchu/Getchu.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/Getchu/Getchu.user.js
// @version     2016.1.1
// @grant       GM_xmlhttpRequest
// ==/UserScript==

if (window.location.href.includes("attestation")) {
    window.location.href = window.location.href.substring(window.location.href.indexOf("?aurl=") + 6) + "&gc=gc"
  //alert()
}

$(".tablebody > div").css({
    "width": 1440,
    "margin": 0,
});

$("img.lazy").each(function() {
    $(this).attr("src", "data-original");
});