﻿// ==UserScript==
// @name        Engadget HTML5 Videos
// @namespace   mattng
// @include     http://www.engadget.com/*
// @version     2014.12.20
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/c4f0d30bb9633dcb8ffca0f64fd9d7c04b7eb43a/Engadget_HTML5_Videos/Engadget_HTML5_Videos.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/c4f0d30bb9633dcb8ffca0f64fd9d7c04b7eb43a/Engadget_HTML5_Videos/Engadget_HTML5_Videos.user.js
// @icon        https://lh4.googleusercontent.com/-Lw5iOOjxJgg/AAAAAAAAAAI/AAAAAAAAhmc/IuX9QVdh0TU/photo.jpg
// @grant       GM_xmlhttpRequest
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// ==/UserScript==

function buildVideoGetUrl(id, resId) {
    var id2 = Math.ceil(parseInt(id, 10) / 100.0);
    return "http://avideos.5min.com/" + id2 + "/" + id + "_" + resId + ".mp4";
}

// Starts here: Detect if any embed-5min video players
$("iframe").each(function(){
	var $self = $(this);
	var src = $self.attr("src");
    var item = "&playList=";
	if (src.contains(item)) {
		var $parent = $self.parent();
		$self.remove();
		var id = src.substring(src.lastIndexOf(item) + item.length);
		if (id) {
            alert( buildVideoGetUrl(id, 8) )
            $parent.append(
                "<video controls='controls' width='100%' height='100%'> \
                    <source src='" + buildVideoGetUrl(id, 8) + "' type='video/mp4'></source> \
                    <source src='" + buildVideoGetUrl(id, 4) + "' type='video/mp4'></source> \
                </video>");
		} else {
			alert("Cannot find the playlist id");
		}
	}
});