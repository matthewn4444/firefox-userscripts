﻿// ==UserScript==
// @name        Nijie Better
// @namespace   none
// @include     http://nijie.info/*
// @include     http://nijie.info
// @include     https://nijie.info/*
// @include     https://nijie.info
// @version     2015.12.2
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/11715a82aad2ff411746858220dc0b4958f7008d/Nijie_Better/Nijie_Better.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/11715a82aad2ff411746858220dc0b4958f7008d/Nijie_Better/Nijie_Better.user.js
// @version     2015.12.2
// @icon        https://lh3.googleusercontent.com/EdqrESDzeGLhJV-G0vxFC0JleA26qzrCjnyUIp6FZkA_NSK-sdBUJlopUmUpSNcKp8A-QA=s114
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var IMAGES = {
    ARROW   : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAYAAADSprJaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZJREFUeNrEmU9okmEcx9/32VtuK+zvVocQQQjSQ6yLhwgC22HDo9cgRHAHRQ8RorliBYVmwVxnhXQnj04Pg0512C5aMA8165IYrZXZXOrm3n6vPe94MTV93+d5+8F37JnOffZ7Pz7+HmWXl5df2e32pXq9/olhmC+QCmQX0oTsQ/hiscgzPcpgMDBKC5nN5qsrKyvzRqNReLQJyCnIcYgGckS4D/whlqFYSPii1+svJZPJ8MzMzBVYTkJOQ7SQUQgHGaEJgsRvtFrtRCQSeexyuW7A8jzkDAYZxx1pg9CAQdKFRqMZ93q9d6PR6C0MchZyAoMcFUCwByw1CKFYlkWzs7M3U6nUnbGxsQvYk5PYk1HcFaIdQb1umJqasmQymUcmk+kiLM/RFBb1u1Gn05kSicQz6ExPYYWuKIUY8Xg89/vdATw5ZrFYrsNl+ry+vr6Nf8x3iWwYtt9GJC2e5w+y2WzS7XYnYLkF+Q75CfmFN7aWBJDc5ZAjrJyOoGF/YRBhhwVBcq4haWH/KaYawg4sJk1hkdLnOAlhEamtV4mwxCCUCCtbTJLCKhaThLCIoVTDCEsNooewk12GJPoQUmGtVutlLKv4rOFUgxBn2HA4/MTn813rhKAmZj9h8/n8U5vNtiSecRDz/4tVFaLZbNbAjXvQhRew3IMcCOHUAqhWq1vBYHA+nU7nYFmDNMRjpioQ5XL5ndPpXNjY2Pgg2bTUg8jlci8dDsdipVIpw/Kb0BTJgbtFFaJj2/6KO1DF2/aeCEANotFo7MZisSjsC6v4daOCL0FdAnA46HA0BAwEAguwVb+F5TYGqOEOtCQdOCyOkoAfMcAPDCC+4XLQbczjVBJwXzJHMMQhhhGw12NwagpIHEKOgEQh5ApIDEKJgIohSAioCIKUgLIhSAooC4K0gEOfRQUBp6enbwPAe+bPe96CAzt4DmhPRaTmU25IAZvif09yQOY6BYzH44uhUGgVA3QVkPSEzkkF9Pv9D6ALbyQC7mCAtoC0jgdtiFKptDk3N/ewUChs4g78JSDN8wm3trb22m63P8cfuoh7wMAfupCo3wIMAPIARF4nP2W8AAAAAElFTkSuQmCC",
};

var CONSTANTS = {
    PREVIEW_MAX_SIZE: 600,
};

var PAGES = {
    LOGIN: {
        name: "login", match: "/login.php",
        run: function() {
            $("#view-tag").translate(TAG_CLOUD);
            $("#content-menu").translate();
        },
    },
    PICTURE: {
        name: "picture", match: "/view.php",
        run: function() {
            $("#view-tag").translate(TAG_CLOUD);
            $("#content-menu").translate();
            
            var username = $("#pro .name").text();
            var userImage = $("#pro img").attr("src");
            var userUrl = $("#pro a.name").attr("href").replace("members", "members_illust");
            
            // Get the main images
            var images = [];
            $("#gallery_open a > img").each(function() {
                images.push(this.src.replace("/__rs_l120x120", "").replace("sam/small-", ""));
            });
            
            // Get images from carousel
            var otherImages = [], otherImagesUrls = [];
            $("#carouselInner-view li img").each(function() {
                otherImagesUrls.push(this.parentNode.getAttribute("href"));
                otherImages.push(this.src);
            });
            
            $("#view-tag").appendTo($("body"));
            $("#view-tag .tag_config").remove();
            
            // Erase the rest of the page
            $("#top, #main, #footer-banner, #bottom, #header-Container").remove();
            
            // Append all images
            for (var i = 0; i < images.length; i++) {
                $("body").append(
                    $("<img>").addClass("box-shadow999 display-image").attr("src", images[i]).attr("id", "image_" + i)
                )
            }
            
            $(".display-image").on("click", function() {
                var i = parseInt(this.id.substring(6), 10);
                var $next = $("#image_" + (i + 1));
                if ($next.length) {
                     $('html,body').animate({
                        scrollTop: $next.offset().top
                    }, 300);
                }
            });
            
            $("body").append($("<div>").attr("id", "other-images"));
            $("#other-images").append(
                $("<a>").addClass("user").attr({"href": userUrl}).append(
                    $("<img>").attr("src", userImage)
                ).append(username)
            );
            for (var i = 0; i < otherImages.length; i++) {
                $("#other-images").append(
                    $("<a>").attr({"target": "_blank", "href": otherImagesUrls[i]}).append(
                        $("<img>").attr("src", otherImages[i].replace("dh=120", "dw=150"))
                    )
                );
            }
        },
    },
    HOME: {
        name: "home",
        match: function(url) {return url.contains("index.php") || url == "http://nijie.info/";},
        run: function() {
            $("P.nijiedao > a").attr({"target": "_blank"});
            $("#mb-tab, #panel .block4, #esi, #index_center_column .indexblock, \
            #index_center_column .mottomiru, \
            #main-right .ranking").translate();
        },
    },
    OKAZU: {
        name: "okazu", match: "/okazu.php",
        run: function() {
            $("#main-full-column .float-right, #main-full-column p:first, .okazu_sort_container").translate();
        },
    },
    ARTICLES: {
        name: "articles", match: "dic/seiten",
        run: function() {
            $("#search_title, #article .index").translate();
            $("#index_main section .info, #dic_title, #breadcrumbs .current").translate(TAG_CLOUD);
        },
    },
    SEARCH: {
        name: "search", match: ["/search.php", "/search_all.php", "like_user_view.php"],
        run: function() {
            $("#content-search-menu, #main-left-main .block-border, #sort_tab").translate();
            $("#search_result .breadcrumb, #main-right .tag").translate(TAG_CLOUD);
            $("#main-left-main .illust_list a, #main-right2 .picture a").newTab();
            applyGalleryPreview("#main-left-main .illust_list .picture img");
        },
    },
    MEMBER_INFO: {
        name: "memberinfo", match: "/members",
        run: function() {
            $("#content-upload-menu, #main-left-none p.mem-indent, #prof-l dt").translate();
            $("#prof-l dd").translate(TAG_CLOUD);
            applyGalleryPreview("#main-left-none .picture img");
        },
    },
};

function main() {
    var url = window.location.href;
    
    var currentPage = null;
    for (var key in PAGES) {
        var match = false;
        if (typeof(PAGES[key].match) === "function") {
            match = PAGES[key].match(url);
        } else if (Array.isArray(PAGES[key].match)) {
            for (var i = 0; i < PAGES[key].match.length; i++) {
                if (url.contains(PAGES[key].match[i])) {
                    match = true;
                    break;
                }
            }
        } else {
            match = url.contains(PAGES[key].match);
        }
        if (match) {
            currentPage = PAGES[key].name;
            PAGES[key].run();
            break;
        }
    }

    if (currentPage) {
        $(document.body).addClass("page-" + currentPage);
        $("#menu-margin, #header-bottom-right, #pro, #main-right2 .index, #main-right2 .side_right_more").translate();
    }
    $("#tag").translate(TAG_CLOUD).find("a").newTab();
    $("#login_slide, #sub-menu, #logo_centering").translate();
}

// Apply css
BRIDGE.css({
    "body": {
        'overflow-x': 'hidden',
    },
    
    // Clean models, ads
    ".login-float, #sprite_syasei_text, #mb-tab li.hitokoto": {
        'display': 'none',
    },
    "#form2": {
        'font-size': '12px',
    },
    
    // Image page
    ".display-image": {
        'display': 'block',
        'margin': '0 auto',
        'padding': '15px',
        'background': 'white',
        'border-radius': '7px',
        'margin-bottom': '40px',
        'margin-top': '40px',
        'cursor': 'default',
        'max-width': '1800px',
    },
    
    "body.page-picture": {
        'padding-left': '200px',
        'padding-top': '20px',
        'padding-bottom': '60px',
    },
    
    "body.page-picture #view-tag": {
        'position': 'absolute',
        'top': 0
    },
    
    "#other-images": {
        'position': 'fixed',
        'top': 0,
        'left': 0,
        'width': '200px',
        'height': '100%',
        'background': '#333',
        'text-align': 'center',
        'overflow-y': 'auto',
    },
    
    "#other-images img": {
        'margin': '0 auto',
        'padding': '10px',
        'border-radius': '3px',
        'display': 'block',
    },
    
    "#other-images a:last-child": {
        'padding-bottom': '60px',
    },
    
    "#other-images .user": {
        'background': 'white',
        'margin': '10px',
        'padding': '5px',
        'border-radius': '7px',
        'font-size': '14px',
        'display': 'block',
    },
    
    "#other-images .user img": {
        'width': '130px',
        'padding': '15px 5px 5px',
        'margin-bottom': '5px',
    },
    
    // Preview Container
    ".previewContainer" : {
        'position'              :   'absolute',
        'display'               :   'none',
        'z-index'               :   109,
        'pointer-events'        :   'none',
    },
    ".previewContainer div" : {
        'position'              :   'absolute'
    },
    ".preview-wrapper:hover .previewContainer": {
        'display'               :   'block',
    },
    ".previewImageHolder" : {
        'position'              :   'relative',
        'padding'               :   '15px',
        'background'            :   '#DDDDDD',
        'box-shadow'            :   '0 0 20px black',
    },
    ".previewArrow" : {
        'background'            :   '-4px -2px url("' + IMAGES.ARROW + '")',
        'width'                 :   '28px',
        'height'                :   '46px',
    },
    ".previewContainer.right .previewArrow" : {
        'right'                 :   '0px',
        'top'                   :   '20px',
    },
    ".previewContainer.right .previewImageHolder" : {
        'top'                   :   0,
        'left'                  :   0,
    },
    ".previewContainer.left .previewArrow" : {
        '-moz-transform'        :   'rotate(180deg)',
        '-moz-transform-origin' :   '50% 50%',
        'left'                  :   0,
        'top'                   :   '20px',
    },
    ".previewContainer.left .previewImageHolder" : {
        'top'                   :   0,
        'left'                  :   '27px',
    },
    ".previewContainer.top .previewArrow" : {
        '-moz-transform'        :   'rotate(270deg)',
        '-moz-transform-origin' :   '50% 31%',
        'left'                  :   '20px',
        'top'                   :   0,
    },
    ".previewContainer.top .previewImageHolder" : {
        'top'                   :   '28px',
        'left'                  :   0,
    },
    ".previewContainer.bottom .previewArrow" : {
        '-moz-transform'        :   'rotate(90deg)',
        '-moz-transform-origin' :   '50% 69%',
        'left'                  :   '20px',
        'bottom'                :   0,
    },
    ".previewContainer.bottom .previewImageHolder" : {
        'bottom'                :   '28px',
        'left'                  :   0,
    },
});

// Translations
var DICTIONARY = {
    "ログイン":           					"Login",
    "ヘルプ":            					"Help",
    "規約":            					"Terms",
    "会員登録":         					"Register",
    "メールアドレス":        					"Email",
    "パスワード":         	 				"Password",
    "保存する":          					"Remember",
    "フォロー":             				"Following",
    "パスワードを忘れた方":   					"Forgot my password",
    "ログイン方法をSSLに変更する": 				"Login with SSL",
    "ニジエに未登録の方":    					"Not registered with Nijie?",
    "ニジエに登録する(無料)": 				"Register with Nijie for free",
    "描け、抜け、学べ":         				"Draw, release and learn",
    "ホーム":              				"Home",
    "投稿":               				"New Post",
    "作品管理":             				"My Work",
    "ブックマーク":           				"Bookmarks",
    "グループ":             				"Groups",
    "おかず":              				"Rankings",
    "大性典":              				"New Articles",
    "気になるタグがある？イラストを検索してみよう！": 	"Looking for a tag? Search for an illustration!",
    "メッセージ":            				"Messages",
    "サブメニュー":           				"Menu",
    "設定":              				"Settings",
    "機能要望":             				"Search",
    "スタンプ投稿":           				"Stamp Posts",
    "プロフ設定":            				"Profile",
    "詳細情報変更":          				"Details",
    "ブロック設定":           				"Block",    
    "アイコン変更":           				"Avatar",
    "ログアウト":            				"Logout",
    "ポップアップ":            				"Popup",
    "お知らせ":             				"Notifications",
    "匿名ボード":            				"Anonymous board",
    "みんなのお題":           				"All Topics",
    "新着の大性典":          				"New Articles",
    "名無しのチンポップ":        				"Unnamed User",
    "プロフィールを変更する":      				"Change Profile",
    "過去のお知らせはこちら":      				"Past Notifications",
    "フォローしてる人の新着2次絵":   			"New Pics You are Following",
    "みんなの2次絵":           				"New Pics From Everyone",
    "旬なタグ ※試験中※":       			"Seasonal Tags",
    "ニジエたん タグがつけられた2次絵": 			"New Pics Tagged by Nijie",
    "みんなの同人":               			"Doujin",
    "最新のお題":                			"Latest Topic",
    "新着のお題":                			"New Themes",
    "みんなの2次絵をもっとみる":        			"See more pictures from everyone",
    "フォローしてる人の新着絵をもっとみる":   		"See more pictures you follow",
    "ニジエたんタグの新着をもっとみる":     		"See more tagged from Nijie",
    "みんなの同人をもっとみる":         			"See more doujin",
    "新着のお題をもっとみる >>":       			"See more themes >>",
    "コメント履歴":                   		"Comments",
    "抜いた履歴":                    		"Logout Times",
    "閲覧履歴":                     		"Browsing",
    "足あと":                      		"History",
    "勲章ランキング":                  		"Ranking",
    "公認イラスト":                   		"Certified Illustrations",
    "最新のおかず(4件)":               		"Lastest Hot Illustrations",
    "ランダムイラスト":                 		"Random illustrations",
    "もっとみる":                    		"See more",
    "おかずをもっとみる":                    	"See more",
    "フォロー中":                    		"Following",
    "フォローを解除":                  		"Unfollow",
    "フォロー解除":                  		"Unfollow",
    "作者のイラスト一覧":                		"Back to Album",
    "フォローする":                   		"Follow",
    "最新のおかず":                   		"Lastest",
    "今日のおかず":                   		"Today",
    "今週のおかず":                   		"This Week",
    "今月のおかず":                   		"This Month",
    "から":                         		" to ",
    "まで":                         		" ",
    "期間指定する":                   		"Search Period",
    "TOPに戻る":                     		"Return to Top",
    "新着記事 (1ページ目)":             	"New Articles (First Page)",
    "記事を書く":                    		"Write an Article",
    "新着記事":					  		"New Articles",
    "記事内容":                     		"Definition",
    "イラスト検索":                   		"Illustrations",
    "同人検索":                     		"Doujin",
    "お題検索":                      		"Topics",
    "ユーザ検索":                    		"User",
    "大性典検索":                    		"Articles",
    "タグ":                         		"Tag",
    "タイトル・本文":                  		"Title",
    "部分一致":                     		"Partial Match",
    "完全一致":                    		"Exact Match",
    "最新の投稿順":                   		"Latest Post",
    "過去の投稿順":                   		"Past Post",
    "いいねが多い順":                     	"Order By Likes",
    "全て":                         		"All",
    "イラスト":                         	"Illustration",
    "漫画":                         		"Manga",
    "アニメ":                          	"Animation",
    "更新記事":                       	"Just Updated",
    "ランダム記事":                    		"Random Article",
    "トップ":                          	"Home",
    "投稿イラスト":							"Illustrations",
    "同人":								"Doujin",
    "お題":								"Topics",
    "一言ボード":							"Board",
    "２次絵":								"Illustrations ",
    "さんのプロフィール":						"'s Profile",
    "名前":								"Name",
    "好きなジャンル":						"Favourite Genre",
    "自己紹介":							"Introduction",
    
};

var TAG_CLOUD = {
    "ﾗﾝﾀﾞﾑﾀｸﾞ":     	"Random",
    "ロリ":      		"Loli",
    "おっぱい":      	"Boobs",
    "ケモノ":      	"Furry",
    "首輪":      	"Collar",
    "Ｍ字":      	"M-shaped",
    "尻":      		"Ass",
    "ダブルピース":    	"Double piece",
    "ポップン":      	"Popping",
    "モイモイ":      	"MoiMoi",
    "東方":      	"Oriental",
    "子宮ノック":      	"Uterus knock",
    "中出し":      	"Cum inside",
    "スケッチブック":   	"Sketchbook",
    "触手":      	"Tentacle",
    "おなか":      	"Stomach",
    "文":      		"Words",
    "みつどもえ":      	"Threesome",
    "幼女":      	"Little girl",
    "ボテ腹":      	"Pregnant",
    "ヤマメ":      	"Trout",
    "ぶっかけ":      	"Bukkake",
    "ニーソ":      	"Knee-socks",
    "子宮口":      	"Cervix",
    "ロリ":      		"Loli",
    "ちっぱい":      	"Small boobs",
    "頬ズリ":      	"Dick to cheeck",
    "ペド":      		"Pedo",
    "獣耳":      	"Moe objects",
    "アナル":      	"Anal",
    "断面":      	"Cross section",
    "全裸":      	"Nude",
    "マイナーキャラ":   	"Minor characters",
    "オナホ妖精":    	"Onaho fairy",
    "ボコォ":      	"Pregnant sounds",
    "ピアス": 			"Piercings",
    "体内貫通":     	"Through the body",
    "フェラ":			"Fellatio",
    "くぱぁ":			"Spread pussy",	
    "放尿":			"Pee",
    "母乳":			"Breast milk",	
    "素股":			"Intercrural sex",
    "腋":			"Armpits",
    "腋毛":			"Underarm hair",
    "オリジナル":		"Original",
    "超乳":			"Huge tits",
    "ふたなり":		"Futanari",
    "陥没乳首":		"Inverted nipple",
    "眼鏡":			"Glasses",
    "手コキ":			"Hand job",
    "褐色":			"Brown skin",
    "お尻":			"Ass",
    "ビキニ":			"Bikini",
    "オナホコキ":		"Using a unahole",
    "看板娘":		"Girl poster",
    "ふともも":			"Thighs",
    "手書き":			"Drawn",
    "金髪":			"Blond",
    "パイパン":		"Shaved",
    "むちむち":		"Voluptuous",
    "エロ下着":		"Sexy underwear",
    "裸ネクタイ":		"Naked tie",
    "イキ顔":			"Horny face",
    "ボンデージ":		"Bondage",
    "ベリーショート":		"Short",
    "スライム娘":		"Slime girl",
    "無修正":		"Uncensored",	

    // Names
    "マーニャ":      	"Maya",
    "ドラクエ":      	"Dragon Quest",
    "ポケモン":      	"Pokemon",
    "フウロ":      	"Skyla",
    "朱鷺子":      	"Tokiko",
    "小箱とたん":     	"Totan Kobako",
    "麻生夏海":    	"Natsumi Asou",
    "古明地さとり":   	"Komeiji Satori",
    "ルーミア":      	"Mia Rou",
    "丸井みつば":   	"Marui Mitsuba",
    "黒谷ヤマメ":    	"Botehara",
    "レモンちゃん":    	"Lemon-chan",
    "ゼロの使い魔":  	"Familar of Zero",
    "ルイズ":      	"Louis",
    "DQ5":      	"Dragon Quest 5",
    "魔理沙":      	"Marisa",
    "バファローベル":   	"Buffalo Bell",
    "ビックリマン2000": 	"Bikkuriman 2000",
    "ミーシャ":     	"Misia",
    "ゼシカ":			"Jessica",
    "ミルキィホームズ":	"Milky Holmes",
    "初音ミク":		"Hatsune Miku",
    "アイドルマスターシンデレラガールズ":	"Idolmaster Cinderella Girls",
    "ストライクウィッチーズ":	"Strike Witches",
    "ニジエたん":		"Nijie-chan",
    "ニジエ":			"Nijie",
    "超次元ゲイムネプテューヌ":	"Hyperdimension Neptunia",
    "ユニ":			"Uni",
    "アリスソフト":		"Alicesoft",
    "ランスシリーズ":		"Rance series",
    "アスカ":			"Asuka",
    "エヴァンゲリオン":	"Evangelion",	
    "ティファ":			"Tifa",
    "パンツ":			"Pants",
};

$.fn.translate = function(dictionary) {
    dictionary = dictionary || DICTIONARY;
    this.each(function() {
        var walker = document.createTreeWalker(this, NodeFilter.SHOW_TEXT, null, false);
        var translatedText = null;
        while (walker.nextNode()) {
            var textNode = walker.currentNode,
                text = textNode.nodeValue.trim();
            if (text.length) {
                if ((translatedText = dictionary[text])) {
                    textNode.nodeValue = translatedText;
                } else if (dictionary == TAG_CLOUD) {
                    //console.log("Missing translation for '" + text + "'");
                }
            }
        }
        // Handle buttons
        var inputs = this.getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) {
            var el = inputs[i];
            var text = null;
            if (el.getAttribute("type") == "submit") {
                text = el.value;
                if ((translatedText = dictionary[text])) {
                    el.value = translatedText;
                } else {
                    //console.log("Missing translation for '" + text + "'");
                }
            } else if ((text = el.getAttribute("placeholder")) != null) {
                if ((translatedText = dictionary[text])) {
                    el.setAttribute("placeholder", translatedText);
                } else {
                    //console.log("Missing translation for '" + text + "'");
                }
            }
        }
    });
    return this;
}

$.fn.newTab = function() {
	return this.attr("target", "_blank");
}

/*===================================*\
|*           Image Preview           *|
\*===================================*/
function previewGalleryImageLoad($li, $img, src, curCol, curRow, totalCols, totalRows) {
    // Preload the medium image
    preloadImage(src, function(el) {
        var $obj = $img.parent(),
            ratio = el.width * 1.0 / el.height,
            height, width;
        $obj.parent().addClass("preview-wrapper");
        
        if (ratio > 1) {
            // Landscape
            width = CONSTANTS.PREVIEW_MAX_SIZE;
            height = Math.round(width / ratio);
        } else {
            // Portrait
            height = CONSTANTS.PREVIEW_MAX_SIZE;
            width = Math.round(height * ratio);
        }
        
        // Insert the image to HTML
        $obj.after(
            $("<div>").addClass("previewContainer").append(
                $("<div>").addClass("previewImageHolder").append(
                    $("<img>").addClass("prevImg").attr("src", el.src).css({
                        width: width, height: height, "max-width": "none"
                    })
                )
            ).append(
                $("<div>").addClass("previewArrow")
            )
        );
        
        // Position it
        var isLeft = curCol < totalCols/2,
            padding = 10;
        
        var $container = $li.find("div.previewContainer:eq(0)"),
            $prvwHolder = $container.find("div.previewImageHolder"),
            $pointer = $container.find("div.previewArrow:eq(0)"),
            
            pointerHeight = $pointer.outerHeight(),
            pointerWidth = $pointer.outerWidth(),
            thumbWidth = $img.outerWidth(),
            thumbHeight = $img.outerHeight(),
            horizOffset = ($li.outerWidth() - thumbWidth) / 2;
            
        // Calculate how far down we set the preview image
        var heightSpace = height - pointerHeight,
            downPercent = curRow * 1.0 / totalRows,
            prevTop = heightSpace * downPercent;
            
            $li.attr("down", downPercent + " " + curRow + " " + totalRows)
            
        if (isLeft) {
            $container.css({top: 0, left: padding + thumbWidth + horizOffset});
          
            // Set the position of the pointer
            $pointer.css({top: (thumbHeight-pointerHeight)/2, transform: "rotate(180deg)"});
            
            // Set the position of the image
            $prvwHolder.css({top: -prevTop, left: pointerWidth -1});
        } else {
            $container.css({top: 0, left: -width-pointerWidth-padding+horizOffset});
          
            // Set the position of the pointer
            $pointer.css({top: (thumbHeight-pointerHeight)/2, left: width});
            
            // Set the position of the image
            $prvwHolder.css({top: -prevTop, left: -padding-pointerWidth+1});
        }
    });
}

function applyGalleryPreview($imgs) {
    $imgs = $($imgs);
    if ($imgs.length) {
        var totalCols = 1, totalRows = 1;

        // Find how many columns and rows there are
        var $li = $imgs.eq(0).parents(".nijie");
        var blockWidth = $li.outerWidth(), parent = $imgs.eq(0).parent();
        while ((totalCols = Math.round(parent.outerWidth() / blockWidth)) <= 1 && !parent.is("#main")) {
            parent = parent.parent();
        }
        totalRows = Math.floor($imgs.length / totalCols);
        
        // Add preview to each image
        $imgs.each(function(i) {
            var $img = $(this),
                src = $img.attr("src");
                
            $img.parent().newTab();
            if (src) {
                var medSrc = src.replace("/__rs_l120x120", "").replace("sam/small-", "");
                previewGalleryImageLoad($img.parents(".nijie"), $img,
                    medSrc, i % totalCols, i / totalCols, totalCols, totalRows);
            }
        });
    }
}

function preloadImage(src, cb, tries) {
    tries = tries || 0;
    var imgEl = new Image();
    imgEl.onload = function() {
        cb(imgEl);
    }
    imgEl.onerror = function() {
        if (tries >= 1) {
            // Give up
            console.log("Cannot load image", src);
        } else {
            // Try again...
            preloadImage(src + "?" + Date.now(), cb, 1 + tries);
        }
    }
    imgEl.src = src;
}

main();
