// ==UserScript==
// @name        pb
// @namespace   matthewn4444
// @version     2015.7.11
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/pb/pb.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/pb/pb.user.js
// @include     *oyplus.com/gallery/*
// @include     *oyplus.com/video/*
// @include     *oyplus.com/?*
// @include			*oyplus.com/
// @include     *oyplus.com/postlogin/
// @include     *oyplus.com/acces*
// @require     http://matthewng.herokuapp.com/userscripts/all.js
// @grant       none
//
// u dink67
// p chiloutchilo3000
// ==/UserScript==

var u = "dink67";
var p = "chiloutchilo3000";

function start() {
    var url = window.location.href;
    $(".bg-skin").remove();

    // Autologin
    if (url.contains("/acces")) {
        $("#username").val(u);
        $("#password").val(p);
        $("#login-form").submit();
    } else if (url.contains("postlogin")) {
        window.location.href = "/";
    } else {
        // Gallery
        $(".thumbnail-listing li a").attr("target", "_blank").each(function(){
            var o = $(this);
            var midImg = o.attr("href");
            var larImg = o.attr("data-image-full");

            o.attr("href", larImg);
        });

        // Attach the HTML5 video
        var vidContainer = $(".video-container");
        if (vidContainer.length) {
            // Change to eq(1) for 720p
            var videoUrl = $(".btn-download li:not(.title):eq(0) a").attr("href");
            if (videoUrl) {
                vidContainer.after(
                    $("<video>").attr({
                        id: "video-player",
                        controls: "controls",
                    }).css({
                        width: "100%",
                        height: "100%",
                    }).append(
                        $("<source>").attr({
                            type: "video/mp4",
                            src: videoUrl
                        })
                    )
                );
                vidContainer.remove();
                $(".video-play-btn").click(function(){
                    $(".video-cover").animate({top: "-100%"});
                    $(".release-poster").animate({height:"584px"}, function(){
                        $("#video-player").get(0).play()
                    });
                });
            } else {
                alert("Video does not exist! '"+ videoUrl + "'");
            }
        }
    }
}

$("#pjax-container").css("visibility", "hidden !important");

BRIDGE.css({
    ".ajax-overlay": {
        "display" : "none !important"
    },
    ".wrapper" : {
        "background": "transparent !important"
    }, 
    " .main, .partner-promos" : {
        "background": "rgba(255,255,255,0.3) !important"
    },
    ".first-content-box, .comment-textarea" : {
        "background": "rgba(255,255,255,0.5) !important"
    },
  	".viewer-container .next, .viewer-container .prev" : {
      	"width" : "40% !important",
    }
});

start();
