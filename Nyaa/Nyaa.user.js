// ==UserScript==
// @name        Nyaa
// @namespace   none
// @version     2018.6.6
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/Nyaa/Nyaa.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/Nyaa/Nyaa.user.js
// @include     https://sukebei.nyaa.si/*
// @exclude     https://sukebei.nyaa.si/view/*
// @require     https://matthewng.herokuapp.com/userscripts/all.js?1
// @grant       GM.xmlHttpRequest
// ==/UserScript==

var CONSTANTS = {
    TILE_SIZE: 200,
    TEST_LIMIT: 0,
    NO_SESSION: false,
  	PARALLEL_JOBS:2,
  	JAV_SITE:"https://avmoo.asia",
};

// Fluid plugin
(function(g){function z(b){h=this;k=g.extend(k,b);b=this.css("position");"relative"!=b&&"absolute"!=b&&this.css("position","relative");y();var a;window.onresize=function(){clearTimeout(a);a=setTimeout(y,100)};window.onscroll=function(){var b=g(window).scrollTop();var a=b-n;if(Math.abs(a)>=Math.max(l/2,k.columnWidth)){n=b;var b=Math.max(n-l,0),v=n+2*l;if(0>a){for(a=m;0<=a&&r(d.eq(a--)).data("top")>=b;);m=a+1;for(a=w;0<=a;){var e=d.eq(a--);if(e.data("top")<v)break;e.hide()}w=a+1}else{for(a=w;a<d.length&&
r(d.eq(a++)).data("top")<=v;);w=a-1;for(a=m;a<d.length;){e=d.eq(a++);if(e.data("top")>b)break;e.hide()}m=a-1}}}}function y(){var b=Math.floor(g(window).outerWidth()/k.columnWidth);if(b!=e&&0<b){var v=h.height();h.height(v);e=b;g(window).outerWidth();l=g(window).height();n=g(window).scrollTop();a=[];u=[];for(b=0;b<e;b++)u[b]=a[b]={col:b,y:0};var c=Math.max(n-l,0),v=n+2*l;d=h.find(k.itemSelector).css({display:"none",position:"absolute",margin:0});for(b=0;b<d.length&&a[a.length-1].y<c;)x(d.eq(b++));
0<b?(m=b-1,r(d.eq(b-1))):m=b;for(;b<d.length&&a[0].y<v;)c=d.eq(b++),x(c),r(c);for(w=b-1;b<d.length;)x(d.eq(b++));h.css({height:a[a.length-1].y,"min-width":e*k.columnWidth})}}function r(b){return b.css({display:"block",top:b.data("top"),left:b.data("left"),width:b.data("width")})}function A(b){var a=b.data("data-original-width"),c=b.data("data-original-height"),c=a/c,a=Math.ceil(a/k.columnWidth),a=g(window).outerWidth()*(a/e),c=a/c;b.outerHeight(c);return[a,c]}function x(b){var d=b.data("data-original-width"),
c=b.data("data-original-height");d||(c=window.getComputedStyle(b.get(0)),d=parseInt(c.width,10),c=parseInt(c.height,10),b.data("data-original-width",d),b.data("data-original-height",c));var c=Math.ceil(d/k.columnWidth),h=a[0].col,g=a[0].y,n=A(b)[1];if(1==c||1==e)a[0].y+=n;else{for(var p=!1,f=0;f<e&&!p&&!(0<f&&a[f-1].y!=a[f].y);f++)if(!(a[f].col+c>u.length)){for(var q=1;q<c;q++)if(p=!0,u[a[f].col+q].y>a[f].y){p=!1;break}if(p){g=a[f].y;h=a[f].col;var t=g+n;for(q=0;q<c;q++)u[a[f].col+q].y=t}}if(!p)for(var l=
1,m=a[l].y;!p;){for(f=0;f<=e-c&&!p;f++){for(var r=f;f<c+r;f++)if(p=!0,u[f].y>m){p=!1;break}if(p){g=m;h=r;t=g+n;for(q=0;q<c;q++)u[r+q].y=t;break}}if(!p)if(l<a.length){for(;l+1<a.length&&m>=a[++l].y;);m=a[l].y}else m+=k.columnWidth}}t={width:h*k.columnWidth,left:0,top:g};k.scaleToWidth&&(1==e?t.width="100%":(t.width=Math.round(1E3/e*c*d/(c*k.columnWidth))/10+"%",t.left=Math.round(1E3*h/e)/10+"%"));b.data(t);a.sort(function(a,b){var c=a.y-b.y;return c?c:a.col-b.col})}var k={columnWidth:100,scaleToWidth:!0,
itemSelector:".item"},h,e=0,a,u,l,n,m,w,d;g.fn.fluid=function(b,e){if("appended"==b){d=h.find(k.itemSelector);x.call(this,e);r.call(this,e);var c=a[a.length-1].y;h.height()<c&&h.css("height",c+"px")}else h||z.call(this,b);return this}})(jQuery);

var TYPE = {
    JAV: 0,
    HANIME: 1,
    HMANGA: 2,
    HGAME: 3,
    fromUrl: function(url) {
        var category = url.substring(url.lastIndexOf("=") + 1);
        if (category == "2_2") {
            return TYPE.JAV;
        } else if (category == "1_1") {
            return TYPE.HANIME;
        } else if (category == "1_4" || category == "1_5" || category == "1_2") {
            return TYPE.HMANGA;
        } else if (category == "1_3") {
            return TYPE.HGAME;
        }
        alert("Cannot find: " + category);
        return null;
    }
};

// Need to replace the reffer in config:about

/*
//  Image Lookup
//      Looks up the larger image from a url given
*/
var IMAGE = {
    hosts: [
        {
            match: ["/upload/small/", "imgwet.com/small/"],
            get: function(url, callback) {
                callback(url.replace("/small/", "/big/"));
            }
        },
        {
            match: ["imgdino.com/images/", "imgtiger.com/images/", ".imgzap.com/images/", "imgdream.net/images/"],
            get: function(url, callback) {
                callback(url.replace("_thumb.", "."));
            }
        },
        {
            match: ["image.imagecrest.com"],
            get: function(url, callback) {
                callback(url.replace("thumbnail/", ""));
            }
        },
        {
            match: [".imgchili.net/"],
            get: function(url, callback) {
                callback(url.replace("//t", "//i"));
            }
        },
        {
            match: ["i.doujinshi.space/s/"],
            get: function(url, callback) {
                callback(url.replace("/s/", "/"));
            }
        },
        {
            match: [".imgclick.net/i/", "imgmega.com/i/", ".imgtrex.com", ".ironimg.net/i/" ],
            get: function(url, callback) {
                callback(url.replace("_t.", "."));
            }
        },
        {
            match: ["directupload.net/images/"],
            get: function(url, callback) {
                callback(url.replace("/temp/", "/"));
            }
        },
        {
            match: ["empireload.com/sexy/Images/thumbs/"],
            get: function(url, callback) {
                callback(url.replace("/thumbs/", "/files/"));
            }
        },
        {
            match: ["empireload.com/sexy/Images/links.php?file="],
            get: function(url, callback) {
                callback(url.replace("/links.php?file=", "/files/"));
            }
        },
        {
            match: ["imagetwist.com/"],
            get: function(url, callback) {
                if (url.includes("/th/")) {
                    callback(url.replace("/th/", "/i/"));
                } else if (url.includes(".jpg") || url.includes(".jpeg") || url.includes(".png")) {
                    SimpleHtmlParser.get(url, function(p) {
                        if (p.containsTextThenSkip("pic img img-responsive")) {
                            callback(p.getAttributeInCurrentTag("src"));
                        } else {
                            console.log("image twist cannot find image on page");
                            callback();
                        }
                    });
                } else {
                    callback();
                }
            }
        },
        {
            match: ["/imglocker.com/"],
            get: function(url, callback) {
                var start = url.indexOf("imglocker.com/");
                if (start == -1) return callback();
                var part = url.substring(start + "imglocker.com/".length);
                return "http://img.imglocker.com/" + part.replace("/", "_").replace(".", "_") + ".html";
                //callback(url.replace("/thumbs/", "/files/"));
            }
        },

        // Requires to get new images
        {
            match: ["www.imgbabes.com/", "/www.imgflare.com"],
            get: function(url, callback) {
                SimpleHtmlParser.get(url, function(p){
                    if (p.containsTextThenSkip('this_image')) {
                        callback(p.getAttributeInCurrentTag("src"));
                    } else {
                        callback();
                    }
                });
            }
        },
        {
            match: ["3xplanet.com"],
            get: function(url, callback) {
                SimpleHtmlParser.get(url, function(p){
                    if (p.containsTextThenSkip("view-content")) {
                        p.skipText(["<img"]);
                        callback(p.getAttributeInCurrentTag("src"));
                    } else {
                        callback();
                    }
                });
            }
        },
        {
            match: ["http://imgrock.net/"],
            get: function(link, callback) {
                 window.get(link, function(data) {
                    var index = data.lastIndexOf("window.location = \"http://imgrock.net/");
                    if (index != -1) {
                        index += 19;
                        var end = data.indexOf("\"", index);
                        url = data.substring(index, end);

                        end = link.lastIndexOf("/");
                        if (end == -1) {
                            callback();
                        }
                        index = link.lastIndexOf("/", end - 1);
                        if (index == -1) {
                            callback();
                        }
                        var id = link.substring(index + 1, end);

                        SimpleHtmlParser.get(url, function(a) {
                            if (a.containsTextThenSkip(' value="1"><input type="hidden" name="')) {
                                var hash = a.getAttributeInCurrentTag("name");
                                 GM.xmlHttpRequest({
                                    method:"POST",
                                    url: url,
                                    headers: {
                                        "Content-Type": "application/x-www-form-urlencoded"
                                    },
                                    data: "op=view&id=" + id + "&" + hash + "=1&pre=1",
                                    overrideMimeType: 'text/plain; charset=x-user-defined',
                                    onload: function(response) {
                                        var parser = new SimpleHtmlParser(response.responseText);
                                        if (parser.containsTextThenSkip('class="pic"')) {
                                            callback(parser.getAttributeInCurrentTag("src"));
                                        } else {
                                            callback();
                                        }
                                    }
                                });
                            } else {
                                callback();
                            }
                        });
                    } else {
                        callback();
                    }
                });
            }
        },
        {
            match: ["http://picexposed.com/", "http://imgpaying.com/"],
            get: function(url, callback) {
                SimpleHtmlParser.get(url, function(p){
                    if (p.containsTextThenSkip("scaleImg(this)")) {
                        callback(p.getAttributeInCurrentTag("src"));
                    } else {
                        callback();
                    }
                });
            }
        },
        {
            match: ["/imgserve.net/img-", "/damimage.com/img-", "/imgslip.com/img-", "/i.imgslip.com/img-", "i.imgseed.com/img-", "ornscreen.xyz/img-", "s.imghost.top/img-5ae4a8c70a056.html"],
            get: function(url, callback) {
                SimpleHtmlParser.get(url, function(p){
                    if (p.containsTextThenSkip("class='centred")) {
                        callback(p.getAttributeInCurrentTag("src"));
                    } else {
                        callback();
                    }
                });
            }
        },
        {
            match: ["ex.photos/image-"],
            get: function(url, callback) {
                SimpleHtmlParser.get(url, function(p){
                    console.log("--->", url)
                    if (p.containsTextThenSkip("post-content")) {
                        p.skipText("<a href");
                        callback(p.getAttributeInCurrentTag("href"));
                    } else {
                        callback();
                    }
                });
            }
        },
        {
            match: ["/imgtiger.com/viewer.php?file="],
            get: function(url, callback) {
                SimpleHtmlParser.get(url, function(p){
                    if (p.containsTextThenSkip("scale(this)")) {
                        callback(p.getAttributeInCurrentTag("src"));
                    } else {
                        callback();
                    }
                });
            }
        },
        {
            match: ["http://urll.us/"],
            get: function(url, callback) {
                SimpleHtmlParser.get(url, function(p) {
                    var html = p.html;
                    if (html.contains("[URL=")) {
                        callback(html.between("[URL=", "]"));
                    } else if (p.containsTextThenSkip("<img")) {
                        callback(p.getAttributeInCurrentTag("src"));
                    } else {
                        alert("Unable to find anything here: http://urll.us/");
                    }
                });
            }
        },
      {
            match: ["http://www.pixsense.net/themes/latest/uploads/small-"],
            get: function(url, callback) {
                callback(url.replace("small-", ""));
            }
        },
        {
            match: [".pixhost.to/thumbs/"],
            get: function(url, callback) {
                callback(url.replace("//t", "//img").replace("/thumbs/", "/images/"));
            }
        },
        {
            // Passthrough
            match: ["://i.imgur.com/", "s.vndb.org", "tinypic.com", "imghost.eu/images", "https://i.postimg.cc/",
            ".imagebam.com", "http://i.imgsafe.org", "a.pomf.se", ".imghost.nu", ".imghost.io", "anonmgur.com",
            ".postimg.org", ".acg12.com/uploads/", "ooo.0o0.ooo/", "https://imghost.io/images/", "pics.dmm.com/digital/",
            "freeimage.info", "imageteam.org/upload/big/", "img169.com/images/", "pics.dmm.co.jp/mono", "pics.dmm.com/mono/movie",
            "j-horse.com/p/photoip", "pics.dmm.co.jp/digital/", "cdn.helloproject.com/img/", "st.cdjapan.co.jp/pictures/",
            "images-na.ssl-images-amazon.com/images"],
            get: function(url, callback) {
                callback(url);
            }
        },
    ],
    fromUrl: function(url, callback) {
        // Loop through each host and look for a match
        for (var i = 0; i < IMAGE.hosts.length; i++) {
            var host = IMAGE.hosts[i],
                match = host.match;
            for (var j = 0; j < match.length; j++) {
                if (url.contains(match[j])) {
                    return host.get(url, callback);
                }
            }
        }
        console.log("Cannot find it!", url);
        callback();     // Failed
    },
    save: function(url, imgs) {
        sessionStorage.setItem(url, imgs.join("|"));
    },
    load: function(url) {
        if (CONSTANTS.NO_SESSION) return null;
        var v = sessionStorage.getItem(url);
        return v ? v.split("|") : null;
    },
};

/*
//  Service
//      Grabs the image by searching an external site by its name different
//      by each category.
*/
var SERVICE = {
    JAVLIB: {
        execute: function(code, callback) {
          	// Doesnt work now because of cloud flare
            var url = "http://www.javlibrary.com/en/vl_searchbyid.php?keyword=" + code;
            SimpleHtmlParser.get(url, function(p){
                if (p.containsTextThenSkip('video_jacket_img"')) {
                    callback([p.getAttributeInCurrentTag("src")]);
                } else {
                    callback();
                }
            });
        }
    },
  	JAVMOO: {
      	execute: function(code, callback) {
          console.log("Code", code);
          	var url = CONSTANTS.JAV_SITE + "/en/search/" + code;
          console.log(url)
          	SimpleHtmlParser.get(url, function(p) {
              	if (p.containsTextThenSkip("movie-box")) {
                  	var link = p.getAttributeInCurrentTag("href");
                  	if (p.containsTextThenSkip("img")) {
                      	return callback([p.getAttributeInCurrentTag("src").replace("ps.", "pl."), link]);
                    }
                }
              	callback();
            });
        }
    },
    DLSITE: {
        execute: function(search, callback) {
            var url = "http://www.dlsite.com/maniax/fsr/=/keyword/" + encodeURIComponent(search);
            SimpleHtmlParser.get(url, function(p) {
                if (p.containsTextThenSkip("work_1col_thumb")) {
                    p.skipText(["img"]);
                    var img = p.getAttributeInCurrentTag("src");
                    var id = img.between("/", "_img_sam.", img.lastIndexOf("/"))
                    return callback([img.replace("_sam.", "_main."),
                        "http://www.dlsite.com/maniax/work/=/product_id/" + id + ".html"]);
                }
                callback();
            });
        }
    },

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    JAV: {
        shouldParseUrl: function(url) {
            return !url.contains(".nyaa.") || url.contains("3xplanet");
        },
        execute: function(name, callback){
            var code = name.match(/([a-zA-Z]{2,}\-[0-9]{3,})/)
            if (code) {
                //SERVICE.JAVLIB.execute(code[0], callback);
              	SERVICE.JAVMOO.execute(code[0], callback);
            } else {
                callback();
            }
        },
    },
    HANIME: {
        shouldParseUrl: function(url) {
            return (!url.endsWith(".html") || url.endsWith(".jpg.html"))
                && !url.contains(".nyaa.")
                && !url.endsWith("/");
        },
        execute: function(name, callback){
            callback();
        },
    },
    HMANGA: {
        execute: function(name, callback){
            callback();
        },
    },
    HGAME: {
        shouldParseUrl: function(url) {
            return (!url.endsWith(".html") || url.endsWith(".jpg.html"))
                && !url.contains(".nyaa.")
                && !url.endsWith("/");
        },
        execute: function(name, callback){
            var isDoujin = name.contains("同人ゲーム");
            var keyword = name.replace(/\[[^\]]+\]+/g, "")
                              .replace(/『[^』]+』+/g, "")
                              .replace(/\([^\)]+\)+/g, "")
                              .replace(/Ver\d.*?$/g, "")
                              .replace(/Dummy Cut.*?$/g, "")
                              .trim();
            console.log("thing", isDoujin, keyword)
            if (isDoujin) {
                return SERVICE.DLSITE.execute(keyword, callback);
            } else {
                SERVICE.DLSITE.execute(keyword, function(imgs){
                    if (imgs) return callback(imgs);
                    callback();
                });
            }
        },
    },
};

function start() {
    var tiler = new Tiler(Item);
    var $layout = tiler.run($("table tr").slice(1));
    $("body > div.container").prepend($layout);

    // Add seeders and leechers button
    $(".center > nav").prepend(
        $("<button>").text("Leechers").addClass("btn btn-primary").css({display: "inline-block", "vertical-align": "top", margin: "20px 15px"})
            .click(function() {
                var category = window.location.href.between("&c=", "&");
                if (category == null) {
                    window.location.href = "https://sukebei.nyaa.si/?q=&f=0&s=leechers&o=desc";
                } else {
                    window.location.href = "https://sukebei.nyaa.si/?q=&f=0&c=" + category + "&s=leechers&o=desc";
                }
            })
    ).append(
        $("<button>").text("Seeders").addClass("btn btn-primary").css({display: "inline-block", "vertical-align": "top", margin: "20px 15px"})
            .click(function() {
                    var start = window.location.href.lastIndexOf("&c=");
                    if (start == -1) {
                        window.location.href = "https://sukebei.nyaa.si/?q=&f=0&s=seeders&o=desc";
                    } else {
                        start += 3;
                        var end = window.location.href.indexOf("&", start);
                        var category = end == -1 ? window.location.href.substring(start) : window.location.href.substring(start, end);
                        window.location.href = "https://sukebei.nyaa.si/?q=&f=0&c=" + category + "&s=seeders&o=desc";
                    }
                })
    );

  $("body > div.container").prepend($(".center > nav").parent())

    // Remove the original layout of data
    $("div.table-responsive, div.alert-info, footer, .servers-cost-money, .servers-cost-money1").remove();
}

function htmlDecode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

/*
//  Item Class
//      Define custom item with the given fields representing each item
*/
function Item() {
    this.name;
    this.torrent;
    this.link;
    this.type;
    this.seeds;
    this.leechers;
    this.size;

    // Get later
    this.element;
}

Item.prototype.parse = function($item) {        // $item is each row ($tr)
    var link = $item.find("td:eq(1) a").attr("target", "_blank");
    this.name = link.text().trim();
    this.torrent = $item.find("td:eq(2) a:first").attr("href");
    this.link = link.attr("href");
    if (this.link.includes("#")) {
        this.link = this.link.substring(0, this.link.indexOf("#"));
    }
    this.type = TYPE.fromUrl($item.find("td:first a").attr("href"));
    this.seeds = $item.find("td:eq(5)").text();
    this.leechers = $item.find("td:eq(6)").text();
    this.size = $item.find("td:eq(3)").text();
}

Item.prototype.getImage = function(callbackFn) {
    // Get service to parse whether to accept links
    var service = null;
    switch(this.type) {
        case TYPE.JAV:
            service = SERVICE.JAV;
            break;
        case TYPE.HANIME:
            service = SERVICE.HANIME;
            break;
        case TYPE.HGAME:
            service = SERVICE.HGAME;
            break;
        default:
            console.log("NOT THERE YET", TYPE.HANIME);
            return callbackFn();
    }

    // Get the page and see if there are any images already
    $.get(this.link, function(html){
        var html = htmlDecode(html.between('="torrent-description"', "</div>").replace(/&#10;/, "\n")) + " ";
        var matches = html.match(/(http[^\)\s]+)[\s\)]/gm);
        if (matches == null) {
            callbackFn();
            return;
        }

        var links = [];
        for (var i = 0; i < matches.length; i++) {
            var link = matches[i].substring(0, matches[i].length - 1);
            if (service && service.shouldParseUrl(link)) {
                if (link.contains("=http")) {
                    link = link.substring(link.indexOf("=http") + 1);
                }
                links.push(link);
            }
        }
        // TODO remove http://i.imgur.com/gHwonT8.png links
        console.log("found", links);

        // Get the image from the data we have
        if (links.length) {
            this.__recursiveGetImageFromUrl(0, links, [], function(imgs){
                if (imgs) {
                    callbackFn(imgs);
                } else {
                    // Cannot find images, so we need to get it from name
                    this.__getImageFromName(callbackFn);
                }
            }.bind(this));
        } else {
            this.__getImageFromName(callbackFn);
        }
    }.bind(this));
}

Item.prototype.build = function() {
    this.element =
        $("<article>").append(
            $("<div>").addClass("description")
                .append($("<a>").text(this.name).attr({href: this.link, target: "_blank"}))
                .append($("<span>").text(" | " + this.size + " | " + this.seeds + "/" + this.leechers).addClass("info"))
        ).append(
            $("<div>").addClass("img-holder")
        );
    return this.element;
}

Item.prototype.getImageElement = function() {
    return this.element.find(".img-holder");
}

Item.prototype.__recursiveGetImageFromUrl = function(i, links, found, callbackFn) {
    if (i >= links.length) {
        if (found.length) {
            // Remove duplicates
            var set = {};
            var foundLinks = [];
            for (var j = 0; j < found.length; j++) {
                var link = found[j].trim();
                if (!set[link]) {
                    set[link] = 1;
                    foundLinks.push(link);
                }
            }
            return callbackFn.call(this, foundLinks);
        } else {
            return callbackFn.call(this, null);
        }
    }
    var link = links[i];
    if (link) {
        IMAGE.fromUrl(link, function(img){
            if (img) {
                found.push(img);
            }
            this.__recursiveGetImageFromUrl(i + 1, links, found, callbackFn);
        }.bind(this));
    } else {
        this.__recursiveGetImageFromUrl(i + 1, links, found, callbackFn);
    }
}

Item.prototype.__getImageFromName = function(callbackFn) {
    //console.log("__getImageFromName")
    // Parse the data to get the main image and preview images
    switch(this.type) {
        case TYPE.JAV:
            SERVICE.JAV.execute(this.name, callbackFn);
            break;
        case TYPE.HGAME:
            SERVICE.HGAME.execute(this.name, callbackFn);
        default:
            console.log("NO handle yet");
            callbackFn();
            break;
    }
}



/*
//  Tiler Class
//      Layout data in a simple class
*/

// Constructor
//  @param Item class
function Tiler(ItemClass) {
    // Check to make sure that the Item class has the functions needed
    var _interface = ["parse", "getImage", "build"];
    for (var i = 0; i < _interface.length; i++) {
        var fnName = _interface[i];
        if (!ItemClass.prototype[fnName]) {
            alert("Your Item class does not have a '" + fnName + "' function!");
            throw new Error("Your Item class does not have a '" + fnName + "' function!");
        }
    }

    // Variables
    this.ItemClass = ItemClass;
    this.layout = null;
    this.items = null;

    this.currentlyRunningJobs = 0;
}

//  Run function
//  @param jQuery list of elements
//  @param a callback with the functions
//  @return Returns the layout jQuery element to be appended
//
//  Item class must have the following interface
//  {
//      parse:function($tr){...},
//      getImage:function(callbackFn){...},           // Run the function and pass the url of image
//      build:function(itemObj){...}
//  }
Tiler.prototype.run = function(queryElements) {
    this.layout = $("<div>").addClass("tiler-container")
        .css({width:"100%", height: "60px"});

    this.layout.fluid({
        columnWidth: CONSTANTS.TILE_SIZE,
        itemSelector: '.tile'
    });
    queryElements = $(queryElements);
    this.items = [];

    // Parse the data
    var length = CONSTANTS.TEST_LIMIT > 0 ? CONSTANTS.TEST_LIMIT : queryElements.length;
    for (var i = 0;  i < length; i++) {
        var item = new this.ItemClass();
        item.parse.call(item, queryElements.eq(i));
        this.items.push(item);
    };

    // Get the image for each object
    this.__recursiveGetImages();

    return this.layout;
}

Tiler.prototype.__recursiveGetImages = function() {
    if (this.items.length && this.currentlyRunningJobs < CONSTANTS.PARALLEL_JOBS) {
        var item = this.items.shift();
        this.currentlyRunningJobs++;
        function gotImages(images) {
            if (images) {
                this.__preload(item, images, 0);
            } else {
                // No image :(
                this.__postImage(item);
            }
        }

        // Grab images from cache
        var imgs = IMAGE.load(item.link);
        if (imgs) {
            gotImages.call(this, imgs);
        } else {
            item.getImage(gotImages.bind(this));
        }

        // Fill up the run queue
        this.__recursiveGetImages();
    } else if (!this.items.length) {
        //unsafeWindow.alert("done")
        //console.log("done loading images")
    }
}

Tiler.prototype.__preload = function(item, images, tries) {
    var img = new Image();
    img.src = images[0];
    img.onload = function() {
        clearTimeout(item.timer);
        this.__postImage(item, images, img.width, img.height);
    }.bind(this);
    item.timer = setTimeout(function(){
        console.log("Failed to load image", images)
        this.__postImage(item, images, img.width, img.height);
    }.bind(this), 5000);
}

Tiler.prototype.__postImage = function(item, imgs, width, height) {
    var $el = item.build().addClass("tile");
    if (imgs) {
        // Adjust size of image
        var size = "_1x1";
        var scale = width * 1.0 / height;
        if (scale > 1.2) {
            size = "_2x1";
        } else if (scale < 0.8) {
            size = "_1x2";
        }
        $el.addClass(size);

        // Add the image
        item.getImageElement().addClass("img").css("background", "url('" + imgs[0] + "')");
        //$el.hide().fadeIn();

        // Add preview
        $el.append(
            $("<div>").addClass("holder").append(
                $("<a>").addClass("preview").attr({
                    href: imgs[1] != null ? imgs[1] : imgs[0],
                    target: "_blank",
                }).text("PREVIEW")
            ).append(
                " | "
            ).append(
                $("<a>").addClass("download").attr({
                    href: item.link.replace("/view/", "/download/") + ".torrent"
                }).text("DOWNLOAD")
            )
        );
        IMAGE.save(item.link, imgs);
    } else {
        $el.addClass("no-image");
    }
    if (CONSTANTS.USE_MASONRY) {
        this.layout.append($el).masonry('appended', $el).masonry();
    } else {
        this.layout.append($el).fluid('appended', $el);
    }
    this.currentlyRunningJobs--;
    this.__recursiveGetImages();
}



BRIDGE.css({
  	"div.container" : {
      	"width": "100% !important",
    },
    ".tiler-container": {
        "position": "absolute",
        'left': 0,
        'margin-bottom': '60px',
    },
    // Each tile
    ".tile": {
        "position": "absolute",
        "box-sizing": "border-box",
        "width": CONSTANTS.TILE_SIZE + "px",
        "height": CONSTANTS.TILE_SIZE + "px",
        "overflow": "hidden",
        "background": "rgba(170, 10, 10, 0.4)",
    },
    ".tile._2x1": {
        "width": (2 * CONSTANTS.TILE_SIZE) + "px",
    },
    ".tile._1x2": {
        "height": (2 * CONSTANTS.TILE_SIZE) + "px",
    },
    ".tile .img": {
        "position": "absolute",
        "top": "0",
        "left": "0",
        "background-size": "cover !important",
        "background-repeat": "no-repeat  !important",
        "background-position": "center  !important",
        "width": "100%",
        "height": "100%",
    },
    ".tile .preview, .tile .download": {
        "color": "white",
    },
    ".tile .holder": {
        "color": "white",
        "border-top-left-radius": "10px",
        "border-top-right-radius": "10px",
        "padding": "10px",
        "text-decoration": "none",
        "background": "rgba(255, 0, 0, 0.4)",
        "opacity": 0,
        "outline": "none",
        "box-shadow": "0 0 7px 1px rgba(0,0,0,0.4)",
        "transition": "transform ease-in 0.2s, opacity ease-in 0.2s",
        "position": "absolute",
        "bottom": 0,
        "left": "50%",
        "transform": "translate(-50%, 100%)",
    },

    ".tile:hover .holder": {
        "opacity": 1,
        "transform": "translate(-50%, 0%)",
    },

    // Specific for these items
    ".tile .description": {
        "transition": "opacity ease-in 0.2s",
        "opacity": 0,
        "top": 0,
        "box-sizing": "border-box",
        "z-index": 1,
        "width": "100%",
        "padding": "10px",
        "position": "absolute",
        "top": "0",
        "left": "0",
        "background": "rgba(255, 255, 255, 0.5)",
    },
    ".tile .description *": {
        "background": "rgb(255, 0, 249, 0.4)",
        "color": "white",
    },
     ".tile:hover .description, .tile.no-image .description": {
        "opacity": 1,
    },

    // Zoom affect
    //"article:hover": {
    //    "transform": "scale(1.1)",
    //    "transition": "transform 0.2s ease-in",
    //    "box-shadow": "0 0 5px black",
    //    "z-index": 100,
    //},
});

start();