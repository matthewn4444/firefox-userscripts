// ==UserScript==
// @name        Pixiv Better
// @namespace   none
// @version     2018.6.8
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/Pixiv_Better/Pixiv_Better.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/Pixiv_Better/Pixiv_Better.user.js
// @icon        https://lh6.googleusercontent.com/-ziPMXtztcAE/AAAAAAAAAAI/AAAAAAAAGws/WlOHPTJ1Hpo/photo.jpg
// @include     https://www.pixiv.net/*
// @include     *pixiv.net/img*
// @include     *.pixiv.net*
// @exclude     *.pixiv.net/search_user.php
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM.xmlHttpRequest
// ==/UserScript==

String.prototype.mediumImage = function() {
  	return this.replace("/c/150x150/", "/");
}

String.prototype.originalImage = function() {
  	return this.replace("_master1200", "").replace("img-master", "img-original");
}




function start() {
  	// Route

  	let url = window.location.href;
  	if (url == "https://www.pixiv.net/") {
  			applyPreview($("a.gtm-illust-recommend-thumbnail-link"), 3);
  	} else if (url.includes("mode=manga")) {
      	$(".full-size-container").each(function() {
          	let $this = $(this);
          	let url = $this.next().attr("data-src");
          	url = url.originalImage();
          	$this.attr("href", url);
        });
    } else if (url.includes("mode=medium")) {
      	$(window).load(function() {
       			let $img = $("figure img");
            let src = $img.attr("src").originalImage();    
          	let $a = $img.parent();
            $img.attr("src", src);      
        });
    } else if (url.includes("member_illust.php?id=")) {
      	applyPreview($(".image-item > a.work"), 4);
    }
}

function applyPreview($items, cols) { 
  	$items.attr("target", "_blank")
    		.each(function(i) {
      			let $this = $(this);      
      
      			// Apply new link to page for manga
      			let link = $this.attr("href");
      			let p;
      			if ($this.hasClass("multiple")) {
              	// Manga
              	link = link.replace("mode=medium", "mode=manga");
              	//$this.attr("href", link);
            }
          
      			wrapElementPreview($this, cols);      			  			
    		});
}

function repositionPreview($el, cols) {  	
  	let $li = $el.parents("li");
    let $prv = $li.find(".preview-container");
  	let $img = $prv.find("img");
  
  	let col = $li.index() % cols;
  	let rows = Math.ceil($li.parent().children().length / cols);
  	let row = Math.floor($li.index() / cols);
  
  	let fullHeight = parseInt($img.css('padding-top'), 0) + parseInt($img.css('padding-bottom'), 0) + $img.get(0).height;
  	let height = fullHeight - $img.prev().outerHeight();
  	let position = Math.round(height * (row/rows));
  
  	let transX = col < cols / 2 ? "34px" : "-100%";
  	$img.css("transform", "translate(" + transX + ", -" + position + "px)");
  	
  
  
  console.log(col, cols, row, rows, position,$img.css('padding-top'),  $img.get(0).height)
}

function wrapElementPreview($el, cols) {
  	let $parent = $el.parent();
    let col = $parent.index() % cols;
  	let rows = Math.ceil($parent.parent().children().length / cols);
  	let row = Math.floor($parent.index() / cols);

  	let $img = $("<img>");
  	var $prv = $("<div>").addClass("preview-container")
    		.append($("<span>").addClass("pointer"))
        .append($img);
  	if (col >= cols/2) {
      	$prv.addClass("right");
    }
  	$parent.append($prv);
  	$parent.addClass("prepare");
  	let src = $el.find("img").attr("data-src").mediumImage();
    loadImage(src)
      	.then(() => {            
      			$parent.removeClass("prepare").addClass("ready");
    				$img.attr("src", src);
      			repositionPreview($el, cols);
    		}).catch(console.error);
}

function loadImage(src) {
  	return new Promise((res, rej) => {
      	let image = new Image();
      	image.onload = res;
      	image.error = rej;
      	image.src = src;
    });
}


BRIDGE.css({
  	"li.prepare > a:first-child": {
      	border: "#CCC 1px solid",
    },
  	"li.ready > a:first-child": {
      	border: "blue 1px solid",
      	transition: "border 0.5s ease-in-out",
    },
  	"li.ready:hover .preview-container": {
      	display: "block",
    },
  	".preview-container": {
      	display: "none",
      	"pointer-events": "none",
    },
  	".preview-container > *" : {
      	position: "absolute",
      	left: 0,
      	top: 0,  
      	"z-index": "1000",
		},
  	".preview-container .pointer" : {
  			width: "100px",
  			height: "100px",
    	  overflow: "hidden",
      	transform: "rotate(-90deg) translate(-25%,110%)",
      	"z-index": "999",
		},
  	".preview-container .pointer:after" : {
      	content: "''",
      	position: "absolute",
        width: "50px",
        height: "50px",
        background: "#CCC",
        transform: "rotate(-45deg)",
      	top: "75px",
        left: "25px",
      	"box-shadow": "1px -1px 10px rgba(0, 0, 0, 0.7)",
		},
  	".preview-container.right .pointer": {
      	transform: "rotate(90deg) translate(25%,35%)",
    },  
  	".preview-container img" : {
      	"z-index": "900",
      	left: "100%",
      	top: "-25%",
      	transform: "translateX(34px)",
				padding: "20px",
      	"background-color": "#CCC",
      	"box-shadow": "0px 0px 10px rgba(0, 0, 0, 0.7)",
    },
  	".preview-container.right img" : {
      	left: "-20%",
      	transform: "translateX(-100%)",
    },
	
});



start()