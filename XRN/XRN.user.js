﻿// ==UserScript==
// @name        XRN
// @namespace   None
// @include     *uren.org/*
// @version     2015.7.11
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/XRN/XRN.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/2cab622b0549c3ea065ef107a21143abf30c0f2a/XRN/XRN.user.js
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       unsafeWindow
// ==/UserScript==

function removeAds() {
    $("iframe, #errorPageContainer, #bannerOut, #bannerIn").remove();
}

removeAds();
$(window).load(function() {
    removeAds();
    setTimeout(removeAds, 1000);
});

var interval = setInterval(function() {
    $("br").each(function(){
        var text = this.nextSibling.nodeValue;
        console.log(text)
        if (text == "Please disable your ad blocker! ") {
            $(this).parent().parent().remove();
            clearInterval(interval);
        }
    });
}, 50);