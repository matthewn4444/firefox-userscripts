// ==UserScript==
// @name        imgfp
// @namespace   none
// @version     2014.12.20
// @updateURL   https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/imgfp/imgfp.user.js
// @downloadURL https://bitbucket.org/matthewn4444/firefox-userscripts/raw/142d60724be95c0683f3a8aff788919e3652a90f/imgfp/imgfp.user.js
// @include     *www.imagefa*
// @include     *ap.to/photo*
// @require     https://matthewng.herokuapp.com/userscripts/all.js
// @grant       GM_xmlhttpRequest
// ==/UserScript==

if (window.location.href.contains("ap.com/photo")) {
    window.location.href = $("img").attr("src");
} else {
    $("#gallery table a").attr("target", "_blank");
}